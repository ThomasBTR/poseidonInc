# Poseidon-app

Project 6 of OpenClassRooms Dev Java Training

## Getting Started

run the project with a command : ``mvn spring-boot:run``

Get tests results through : ``mvn clean verify``

Install the jar-with-dependencies with :  ``mvn clean install``

## Technical:

1. Framework: Spring Boot v2.7.0
2. Java 11.0.15 Eclipse Temurin
3. Thymeleaf
4. Bootstrap v.5.1.3

## Projet setup :

1. Add lib repository into pom.xml
2. Following data is available on folders :
   - Source root: src/main/java
   - View: src/main/resources/templates
   - Static: src/main/resource/static
3. application.properties contains all the properties regarding spring, database and more
4. Database is available in src/main/resources/database with 2 files : schema.sql for database init and data.sql to
   populate the database
5. Custom Checkstyle is available in the resources
6. openapi.yaml is available with all the Restfull API path

## Git Repo

The git repo is here : https://gitlab.com/ThomasBTR/poseidonInc/

All the branches are up even though all of them have been merged on the main branch with the release version.

You can check to all the pipelines failed and passed here : https://gitlab.com/ThomasBTR/poseidonInc/-/pipelines

## Coverage and Cie

Get all the infos on the current status of the project with SonarCloud :
https://sonarcloud.io/project/overview?id=ThomasBTR_poseidonInc

You can check each branches here : https://sonarcloud.io/project/branches_list?id=ThomasBTR_poseidonInc

## Security

1. The app support OAuth2 authentication with GitHub and Google
2. 2 users are available on the database :
   - (U$er2022) is the password for user
   - (@dMin2022) is the password for admin
3. Each password requires :
   - At least 8 characters,
   - At least 1 lower case character
   - At least 1 Upper case character
   - At least 1 Special character
4. All the password are encrypted before being saved in the database
5. No decryption is available through the RestFull API (password will be returned encrypted) to ensure protection.
