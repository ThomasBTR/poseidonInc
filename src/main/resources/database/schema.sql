CREATE TABLE T_BID_LIST
(
    id           INT         NOT NULL AUTO_INCREMENT,
    account      VARCHAR(30) NOT NULL,
    type         VARCHAR(30) NOT NULL,
    bidQuantity  DOUBLE,
    askQuantity  DOUBLE,
    bid          DOUBLE,
    ask          DOUBLE,
    benchmark    VARCHAR(125),
    bidListDate  TIMESTAMP,
    commentary   VARCHAR(125),
    security     VARCHAR(125),
    status       VARCHAR(10),
    trader       VARCHAR(125),
    book         VARCHAR(125),
    creationName VARCHAR(125),
    creationDate TIMESTAMP,
    revisionName VARCHAR(125),
    revisionDate TIMESTAMP,
    dealName     VARCHAR(125),
    dealType     VARCHAR(125),
    sourceListId VARCHAR(125),
    side         VARCHAR(125),

    PRIMARY KEY (id)
);

CREATE TABLE T_TRADE
(
    id           INT         NOT NULL AUTO_INCREMENT,
    account      VARCHAR(30) NOT NULL,
    type         VARCHAR(30) NOT NULL,
    buyQuantity  DOUBLE,
    sellQuantity DOUBLE,
    buyPrice     DOUBLE,
    sellPrice    DOUBLE,
    tradeDate    TIMESTAMP,
    security     VARCHAR(125),
    status       VARCHAR(10),
    trader       VARCHAR(125),
    benchmark    VARCHAR(125),
    book         VARCHAR(125),
    creationName VARCHAR(125),
    creationDate TIMESTAMP,
    revisionName VARCHAR(125),
    revisionDate TIMESTAMP,
    dealName     VARCHAR(125),
    dealType     VARCHAR(125),
    sourceListId VARCHAR(125),
    side         VARCHAR(125),

    PRIMARY KEY (id)
);

CREATE TABLE T_CURVE_POINT
(
    id           INT NOT NULL AUTO_INCREMENT,
    curveId      tinyint NOT NULL,
    asOfDate     TIMESTAMP,
    term         DOUBLE,
    curveValue   DOUBLE,
    creationDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),

    PRIMARY KEY (id)
);

CREATE TABLE T_RATING
(
    id           INT NOT NULL AUTO_INCREMENT,
    moodysRating VARCHAR(125),
    sAndPRating  VARCHAR(125),
    fitchRating  VARCHAR(125),
    orderNumber  tinyint,

    PRIMARY KEY (id)
);

CREATE TABLE T_RULE_NAME
(
    id           INT NOT NULL AUTO_INCREMENT,
    name         VARCHAR(125),
    description  VARCHAR(125),
    ruleNameJson VARCHAR(125),
    template     VARCHAR(512),
    sqlStr       VARCHAR(125),
    sqlPart      VARCHAR(125),

    PRIMARY KEY (id)
);

CREATE TABLE T_USER
(
    id       INT     NOT NULL AUTO_INCREMENT,
    username VARCHAR(125),
    password VARCHAR(125),
    fullName VARCHAR(125),
    userRole VARCHAR(125),
    enabled  TINYINT NOT NULL DEFAULT 1,

    PRIMARY KEY (id)
);
