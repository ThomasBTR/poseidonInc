insert into T_USER (id, full_name, username, password, role, enabled)
values (0, 'Administrator', 'admin', '$2a$10$49j0YdKIsPPdpn3u.bBRuOhZt1Ldboiq.4lP5jx9sPLIyL43N5aa6', 'ROLE_ADMIN', 1),
       (1, 'User', 'user', '$2a$10$jN0HquqlLgEPNtN4k6Uccu.gGKexFdH7MJe4M/dbtiYnAs5e.Xvxu', 'ROLE_USER', 1);


insert into T_BID_LIST (id, account, type, bid_Quantity, ask_Quantity, bid, ask, benchmark, bid_List_Date, commentary, security, status,
                        trader, book, creation_Name, Creation_Date, revision_Date, deal_Name, deal_Type, source_List_Id, side)
values (0, 'roberto', 'grade_A', 100.0, 20.0, 50.0, 20.0, 'albertho', null, null, null, null, null, null, null, null, null, null, null, null, null),
       (1, 'roberta', 'grade_B', 100.0, 20.0, 50.0, 20.0, 'albertho', null, null, null, null, null, null, null, null, null, null, null, null, null),
       (2, 'albertho', 'grade_C', 100.0, 20.0, 50.0, 20.0, 'albertho', null, null, null, null, null, null, null, null, null, null, null, null, null),
       (3, 'albertha', 'grade_D', 100.0, 20.0, 50.0, 20.0, 'albertho', null, null, null, null, null, null, null, null, null, null, null, null, null),
       (4, 'argentik', 'grade_E', 100.0, 20.0, 50.0, 20.0, 'albertho', null, null, null, null, null, null, null, null, null, null, null, null, null);

insert into T_TRADE (id, account, type, buy_Quantity, sell_Quantity, buy_Price, sell_Price, trade_Date, security, status, trader, benchmark, book, creation_Name,
                     creation_Date, revision_Name, revision_Date, deal_Name, deal_Type, source_List_Id, side)
values (0, 'robert0', 'grade_A', 100.0, 20.0, 50.0, 20.0, '2022-06-13 11:10:39', null, null, null, null, null, null, null, null, null, null, null, null, null),
       (1, 'roberta', 'grade_B', 100.0, 20.0, 50.0, 20.0, '2022-06-13 11:10:39', null, null, null, null, null, null, null, null, null, null, null, null, null),
       (2, 'James', 'grade_C', 100.0, 20.0, 50.0, 20.0, '2022-06-13 11:10:39', null, null, null, null, null, null, null, null, null, null, null, null, null),
       (3, 'Flipper', 'grade_D', 100.0, 20.0, 50.0, 20.0, '2022-06-13 11:10:39', null, null, null, null, null, null, null, null, null, null, null, null, null),
       (4, 'Karim', 'grade_E', 100.0, 20.0, 50.0, 20.0, '2022-06-13 11:10:39', null, null, null, null, null, null, null, null, null, null, null, null, null);

insert into T_CURVE_POINT (id, as_Of_Date, term, curve_Value, creation_Date)
values (0, null, 100.0, 20.0, null),
       (1, null, 200.0, 20.0, null),
       (2, null, 300.0, 20.0, null),
       (3, null, 400.0, 20.0, null),
       (4, null, 50.0, 20.0, null);

insert into T_RATING (id, moodys_Rating, s_AndPRating, fitch_Rating, order_Number)
values (0, 'ABC', 'A', 'A', 0),
       (1, 'AA', 'AA', 'AAA', 1),
       (2, 'BB', 'B', 'B', 2),
       (3, 'CCC', 'CCC', 'C', 3),
       (4, 'A', 'BBB', 'AA', 4);

insert into T_RULE_NAME (id, name, description, rule_Name_Json, template, sql_Str, sql_Part)
values (0, 'Gerry', 'Gerry rule', null, null, null, null),
       (1, 'Tom', 'Tom rule', null, null, null, null),
       (2, 'Jarod', 'Jarod rule', null, null, null, null),
       (3, 'Kendrick', 'Kendrick rule', null, null, null, null),
       (4, 'Jericho', 'Jericho rule', null, null, null, null);