package inc.poseidon.exceptions;

public class PoseidonRuntimeException extends RuntimeException{
    public PoseidonRuntimeException(String s) {
        super(s);
    }

    public PoseidonRuntimeException (final String s, final Throwable throwable) {
        super(s, throwable);
    }
}
