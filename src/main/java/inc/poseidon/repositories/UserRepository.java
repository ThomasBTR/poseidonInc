package inc.poseidon.repositories;

import inc.poseidon.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * The interface User repository.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {

    /**
     * Find user by username user.
     *
     * @param username the username
     * @return the user
     */
    @Query("SELECT user FROM User user WHERE user.username LIKE %:variable%")
    User findUserByUsername(@Param("variable")String username);

}
