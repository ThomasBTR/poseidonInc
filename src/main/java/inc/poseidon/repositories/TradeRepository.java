package inc.poseidon.repositories;

import inc.poseidon.domain.Trade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The interface Trade repository.
 */
@Repository
public interface TradeRepository extends JpaRepository<Trade, Integer> {
}
