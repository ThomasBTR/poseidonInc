package inc.poseidon.repositories;

import inc.poseidon.domain.Bid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The interface Bid list repository.
 */
@Repository
public interface BidListRepository extends JpaRepository<Bid, Integer> {
}
