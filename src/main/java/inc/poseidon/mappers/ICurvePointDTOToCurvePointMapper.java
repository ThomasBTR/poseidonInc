package inc.poseidon.mappers;

import inc.poseidon.domain.CurvePoint;
import inc.poseidon.domain.CurvePointDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * The interface Curve point dto to curve point mapper.
 */
@Mapper
public interface ICurvePointDTOToCurvePointMapper {

    /**
     * The constant INSTANCE.
     */
    ICurvePointDTOToCurvePointMapper INSTANCE = Mappers.getMapper(ICurvePointDTOToCurvePointMapper.class);

    /**
     * Source to destination curve point.
     *
     * @param curvePointDTO the curve point dto
     * @return the curve point
     */
    @Mapping(target = "creationDate", ignore = true)
    @Mapping(target = "asOfDate", ignore = true)
    @Mapping(target = "curveValue", source = "value")
    CurvePoint sourceToDestination(CurvePointDTO curvePointDTO);


    /**
     * Destination to source curve point dto.
     *
     * @param curvePoint the curve point
     * @return the curve point dto
     */
    @Mapping(target = "value", source = "curveValue")
    CurvePointDTO destinationToSource(CurvePoint curvePoint);
}
