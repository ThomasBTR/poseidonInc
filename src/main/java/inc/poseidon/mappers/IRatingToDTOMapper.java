package inc.poseidon.mappers;

import inc.poseidon.domain.Rating;
import inc.poseidon.domain.RatingDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * The interface Rating to dto mapper.
 */
@Mapper
public interface IRatingToDTOMapper {

    /**
     * The constant INSTANCE.
     */
    IRatingToDTOMapper INSTANCE = Mappers.getMapper(IRatingToDTOMapper.class);


    /**
     * Source to destination rating.
     *
     * @param ratingDTO the rating dto
     * @return the rating
     */

    @Mapping(target = "SAndPRating", source = "sAndPRating")
    Rating sourceToDestination(RatingDTO ratingDTO);


    /**
     * Destination to source rating dto.
     *
     * @param rating the rating
     * @return the rating dto
     */

    @Mapping(target = "sAndPRating", source = "SAndPRating")
    RatingDTO destinationToSource(Rating rating);
}
