package inc.poseidon.mappers;

import inc.poseidon.domain.Bid;
import inc.poseidon.domain.BidDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * The interface Bid dto to bid mapper.
 */
@Mapper
public interface IBidDTOToBidMapper {

    /**
     * The constant INSTANCE.
     */
    IBidDTOToBidMapper INSTANCE = Mappers.getMapper(IBidDTOToBidMapper.class);


    /**
     * Source to destination bid.
     *
     * @param bidDTO the bid dto
     * @return the bid
     */
    @Mapping(target = "bidValue", ignore = true)
    @Mapping(target = "trader", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "sourceListId", ignore = true)
    @Mapping(target = "side", ignore = true)
    @Mapping(target = "security", ignore = true)
    @Mapping(target = "revisionName", ignore = true)
    @Mapping(target = "revisionDate", ignore = true)
    @Mapping(target = "dealType", ignore = true)
    @Mapping(target = "dealName", ignore = true)
    @Mapping(target = "creationName", ignore = true)
    @Mapping(target = "creationDate", ignore = true)
    @Mapping(target = "commentary", ignore = true)
    @Mapping(target = "book", ignore = true)
    @Mapping(target = "bidListDate", ignore = true)
    @Mapping(target = "benchmark", ignore = true)
    @Mapping(target = "askQuantity", ignore = true)
    @Mapping(target = "ask", ignore = true)
    @Mapping(target = "bidId", source = "id")
    Bid sourceToDestination(BidDTO bidDTO);

    /**
     * Destination to source bid dto.
     *
     * @param bid the bid
     * @return the bid dto
     */
    @Mapping(target = "id", source = "bidId")
    BidDTO destinationToSource(Bid bid);
}
