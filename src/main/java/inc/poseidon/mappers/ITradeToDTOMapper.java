package inc.poseidon.mappers;

import inc.poseidon.domain.Trade;
import inc.poseidon.domain.TradeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * The interface Rating to dto mapper.
 */
@Mapper
public interface ITradeToDTOMapper {

    /**
     * The constant INSTANCE.
     */
    ITradeToDTOMapper INSTANCE = Mappers.getMapper(ITradeToDTOMapper.class);


    /**
     * Source to destination trade.
     *
     * @param tradeDTO the trade dto
     * @return the trade
     */
    @Mapping(target = "trader", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "sourceListId", ignore = true)
    @Mapping(target = "side", ignore = true)
    @Mapping(target = "security", ignore = true)
    @Mapping(target = "revisionName", ignore = true)
    @Mapping(target = "revisionDate", ignore = true)
    @Mapping(target = "dealType", ignore = true)
    @Mapping(target = "dealName", ignore = true)
    @Mapping(target = "creationName", ignore = true)
    @Mapping(target = "creationDate", ignore = true)
    @Mapping(target = "book", ignore = true)
    @Mapping(target = "benchmark", ignore = true)
    Trade sourceToDestination(TradeDTO tradeDTO);


    /**
     * Destination to source trade dto.
     *
     * @param trade the trade
     * @return the trade dto
     */
    TradeDTO destinationToSource(Trade trade);
}
