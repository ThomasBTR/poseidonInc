package inc.poseidon.mappers;

import inc.poseidon.domain.RuleName;
import inc.poseidon.domain.RuleNameDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * The interface Rating to dto mapper.
 */
@Mapper
public interface IRuleNameToDTOMapper {

    /**
     * The constant INSTANCE.
     */
    IRuleNameToDTOMapper INSTANCE = Mappers.getMapper(IRuleNameToDTOMapper.class);


    /**
     * Source to destination rating.
     *
     * @param ruleNameDTO the rating dto
     * @return the rating
     */
    RuleName sourceToDestination(RuleNameDTO ruleNameDTO);


    /**
     * Destination to source rule name dto.
     *
     * @param ruleName the rule name
     * @return the rule name dto
     */
    RuleNameDTO destinationToSource(RuleName ruleName);
}
