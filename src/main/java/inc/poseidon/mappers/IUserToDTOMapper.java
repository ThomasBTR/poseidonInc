package inc.poseidon.mappers;

import inc.poseidon.domain.User;
import inc.poseidon.domain.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * The interface Rating to dto mapper.
 */
@Mapper
public interface IUserToDTOMapper {

    /**
     * The constant INSTANCE.
     */
    IUserToDTOMapper INSTANCE = Mappers.getMapper(IUserToDTOMapper.class);


    /**
     * Source to destination user.
     *
     * @param userDTO the user dto
     * @return the user
     */
    @Mapping(target = "enabled", ignore = true)
    User sourceToDestination(UserDTO userDTO);


    /**
     * Destination to source user dto.
     *
     * @param user the user
     * @return the user dto
     */
    UserDTO destinationToSource(User user);
}
