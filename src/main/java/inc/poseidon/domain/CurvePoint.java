package inc.poseidon.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;


/**
 * The type Curve point.
 */
@AllArgsConstructor
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "T_CURVE_POINT")
public class CurvePoint {

    /**
     * The Id.
     */
    @Id
    @Column(name = "id")
    private Integer id;

    /**
     * The As of date.
     */
    @Column(name = "asOfDate")
    private LocalDateTime asOfDate;
    /**
     * The Term.
     */
    @NotNull
    @Column(name = "term")
    private double term;

    /**
     * The Curve value.
     */
    @NotNull
    @Column(name = "curveValue")
    private double curveValue;

    /**
     * The Creation date.
     */
    @Column(name = "creationDate")
    private LocalDateTime creationDate;


    /**
     * Instantiates a new Curve point.
     *
     * @param id         the id
     * @param term       the term
     * @param curveValue the curve value
     */
    public CurvePoint(final int id, final double term, final double curveValue) {
        this.id = id;
        this.term = term;
        this.curveValue = curveValue;
        this.asOfDate = LocalDateTime.now();
        this.creationDate = LocalDateTime.now();
    }

    /**
     * Equals boolean.
     *
     * @param o the o
     * @return the boolean
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        CurvePoint that = (CurvePoint) o;
        return id != null && Objects.equals(id, that.id);
    }

    /**
     * Hash code int.
     *
     * @return the int
     */
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
