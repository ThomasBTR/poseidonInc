package inc.poseidon.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;


/**
 * The type Rating.
 */
@AllArgsConstructor
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "T_RATING")
public class Rating {

    /**
     * The Id.
     */
    @Id
    @Column(name = "id")
    private Integer id;

    /**
     * The Moodys rating.
     */
    @Column(name = "moodysRating")
    private String moodysRating;

    /**
     * The S and p rating.
     */
    @Column(name = "sAndPRating")
    private String sAndPRating;

    /**
     * The Fitch rating.
     */
    @Column(name = "fitchRating")
    private String fitchRating;

    /**
     * The Order number.
     */
    @Column(name = "orderNumber")
    private int orderNumber;


    /**
     * Instantiates a new Rating.
     *
     * @param id           the id
     * @param moodySRating the moody s rating
     * @param sandPRating  the sand p rating
     * @param fitchRating  the fitch rating
     * @param orderNumber  the order number
     */
    public Rating(final int id, final String moodySRating, final String sandPRating, final String fitchRating, final int orderNumber) {
        this.id = id;
        this.moodysRating = moodySRating;
        this.sAndPRating = sandPRating;
        this.fitchRating = fitchRating;
        this.orderNumber = orderNumber;
    }

    /**
     * Equals boolean.
     *
     * @param o the o
     * @return the boolean
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        Rating rating = (Rating) o;
        return id != null && Objects.equals(id, rating.id);
    }

    /**
     * Hash code int.
     *
     * @return the int
     */
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
