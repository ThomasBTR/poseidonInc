package inc.poseidon.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The type Rule name.
 */
@AllArgsConstructor
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "T_RULE_NAME")
public class RuleName {

    /**
     * The Id.
     */
    @Id
    @Column(name = "id")
    private int id;

    /**
     * The Name.
     */
    @Column(name = "name")
    private String name;

    /**
     * The Description.
     */
    @Column(name = "description")
    private String description;

    /**
     * The Rule name json.
     */
    @Column(name = "ruleNameJson")
    private String ruleNameJson;

    /**
     * The Template.
     */
    @Column(name = "template")
    private String template;

    /**
     * The Sql str.
     */
    @Column(name = "sqlStr")
    private String sqlStr;

    /**
     * The Sql part.
     */
    @Column(name = "sqlPart")
    private String sqlPart;

    /**
     * Equals boolean.
     *
     * @param o the o
     * @return the boolean
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null) {
            Hibernate.getClass(this);
            Hibernate.getClass(o);
        }
        return false;
    }

    /**
     * Hash code int.
     *
     * @return the int
     */
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
