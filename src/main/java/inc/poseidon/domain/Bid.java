package inc.poseidon.domain;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Objects;

/**
 * The type Bid list.
 */
@ToString
@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Table(name = "T_BID_LIST")
public class Bid {

    /**
     * The Bid id.
     */
    @Id
    @NotNull
    @Column(name = "id")
    private Integer bidId;

    /**
     * The Account.
     */
    @NotNull
    @Column(name = "account")
    private String account;

    /**
     * The Type.
     */
    @NotNull
    @Column(name = "type")
    private String type;

    /**
     * The Bid quantity.
     */
    @NotNull
    @NumberFormat
    @Column(name = "bidQuantity")
    private double bidQuantity;

    /**
     * The Ask quantity.
     */
    @Column(name = "askQuantity")
    private double askQuantity;

    /**
     * The Bid value.
     */
    @Column(name = "bid")
    private double bidValue;

    /**
     * The Ask.
     */
    @Column(name = "ask")
    private double ask;

    /**
     * The Benchmark.
     */
    @Column(name = "benchmark")
    private String benchmark;

    /**
     * The Bid list date.
     */
    @Column(name = "bidListDate")
    private Instant bidListDate;

    /**
     * The Commentary.
     */
    @Column(name = "commentary")
    private String commentary;

    /**
     * The Security.
     */
    @Column(name = "security")
    private String security;

    /**
     * The Status.
     */
    @Column(name = "status")
    private String status;

    /**
     * The Trader.
     */
    @Column(name = "trader")
    private String trader;

    /**
     * The Book.
     */
    @Column(name = "book")
    private String book;

    /**
     * The Creation name.
     */
    @Column(name = "creationName")
    private String creationName;

    /**
     * The Creation date.
     */
    @Column(name = "creationDate")
    private Instant creationDate;

    /**
     * The Revision name.
     */
    @Column(name = "revisionName")
    private String revisionName;

    /**
     * The Revision date.
     */
    @Column(name = "revisionDate")
    private Instant revisionDate;

    /**
     * The Deal name.
     */
    @Column(name = "dealName")
    private String dealName;

    /**
     * The Deal type.
     */
    @Column(name = "dealType")
    private String dealType;

    /**
     * The Source list id.
     */
    @Column(name = "sourceListId")
    private String sourceListId;

    /**
     * The Side.
     */
    @Column(name = "side")
    private String side;


    /**
     * Instantiates a new Bid.
     *
     * @param id          the id
     * @param account     the account
     * @param type        the type
     * @param bidQuantity the bid quantity
     */
    public Bid(final int id, final String account, final String type, final double bidQuantity) {
        this.bidId = id;
        this.account = account;
        this.type = type;
        this.bidQuantity = bidQuantity;

    }

    /**
     * Equals boolean.
     *
     * @param o the o
     * @return the boolean
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        Bid bid = (Bid) o;
        return bidId != null && Objects.equals(bidId, bid.bidId);
    }

    /**
     * Hash code int.
     *
     * @return the int
     */
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public void setBidId(Integer bidId) {
        this.bidId = bidId;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setBidQuantity(double bidQuantity) {
        this.bidQuantity = bidQuantity;
    }

    public void setAskQuantity(double askQuantity) {
        this.askQuantity = askQuantity;
    }

    public void setBidValue(double bidValue) {
        this.bidValue = bidValue;
    }

    public void setAsk(double ask) {
        this.ask = ask;
    }

    public void setBenchmark(String benchmark) {
        this.benchmark = benchmark;
    }

    public void setBidListDate(Instant bidListDate) {
        this.bidListDate = bidListDate;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }

    public void setSecurity(String security) {
        this.security = security;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTrader(String trader) {
        this.trader = trader;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public void setCreationName(String creationName) {
        this.creationName = creationName;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }

    public void setRevisionName(String revisionName) {
        this.revisionName = revisionName;
    }

    public void setRevisionDate(Instant revisionDate) {
        this.revisionDate = revisionDate;
    }

    public void setDealName(String dealName) {
        this.dealName = dealName;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public void setSourceListId(String sourceListId) {
        this.sourceListId = sourceListId;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public Integer getBidId() {
        return bidId;
    }

    public String getAccount() {
        return account;
    }

    public String getType() {
        return type;
    }

    public double getBidQuantity() {
        return bidQuantity;
    }

    public double getAskQuantity() {
        return askQuantity;
    }

    public double getBidValue() {
        return bidValue;
    }

    public double getAsk() {
        return ask;
    }

    public String getBenchmark() {
        return benchmark;
    }

    public Instant getBidListDate() {
        return bidListDate;
    }

    public String getCommentary() {
        return commentary;
    }

    public String getSecurity() {
        return security;
    }

    public String getStatus() {
        return status;
    }

    public String getTrader() {
        return trader;
    }

    public String getBook() {
        return book;
    }

    public String getCreationName() {
        return creationName;
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public String getRevisionName() {
        return revisionName;
    }

    public Instant getRevisionDate() {
        return revisionDate;
    }

    public String getDealName() {
        return dealName;
    }

    public String getDealType() {
        return dealType;
    }

    public String getSourceListId() {
        return sourceListId;
    }

    public String getSide() {
        return side;
    }
}
