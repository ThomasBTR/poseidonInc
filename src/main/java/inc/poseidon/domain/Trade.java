package inc.poseidon.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.Instant;


/**
 * The type Trade.
 */
@AllArgsConstructor
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "T_TRADE")
public class Trade {

    /**
     * The Id.
     */
    @Id
    @NotNull
    @Column(name = "id")
    private int id;

    /**
     * The Account.
     */
    @NotNull
    @Column(name = "account")
    private String account;

    /**
     * The Type.
     */
    @NotNull
    @Column(name = "type")
    private String type;

    /**
     * The Buy quantity.
     */
    @Column(name = "buyQuantity")
    private double buyQuantity;

    /**
     * The Sell quantity.
     */
    @Column(name = "sellQuantity")
    private double sellQuantity;

    /**
     * The Buy price.
     */
    @Column(name = "buyPrice")
    private double buyPrice;

    /**
     * The Sell price.
     */
    @Column(name = "sellPrice")
    private double sellPrice;

    /**
     * The Trade date.
     */
    @Column(name = "tradeDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX", timezone = "UTC")
    private Instant tradeDate;

    /**
     * The Security.
     */
    @Column(name = "security")
    private String security;

    /**
     * The Status.
     */
    @Column(name = "status")
    private String status;

    /**
     * The Trader.
     */
    @Column(name = "trader")
    private String trader;

    /**
     * The Benchmark.
     */
    @Column(name = "benchmark")
    private String benchmark;

    /**
     * The Book.
     */
    @Column(name = "book")
    private String book;

    /**
     * The Creation name.
     */
    @Column(name = "creationName")
    private String creationName;

    /**
     * The Creation date.
     */
    @Column(name = "creationDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX", timezone = "UTC")
    private Instant creationDate;

    /**
     * The Revision name.
     */
    @Column(name = "revisionName")
    private String revisionName;

    /**
     * The Revision date.
     */
    @Column(name = "revisionDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX", timezone = "UTC")
    private Instant revisionDate;

    /**
     * The Deal name.
     */
    @Column(name = "dealName")
    private String dealName;

    /**
     * The Deal type.
     */
    @Column(name = "dealType")
    private String dealType;

    /**
     * The Source list id.
     */
    @Column(name = "sourceListId")
    private String sourceListId;

    /**
     * The Side.
     */
    @Column(name = "side")
    private String side;

    /**
     * Instantiates a new Trade.
     *
     * @param id      the id
     * @param account the account
     * @param type    the type
     */
    public Trade(final int id, final String account, final String type) {
        this.account = account;
        this.type = type;
        this.id = id;
        this.tradeDate = Instant.now();
    }


    /**
     * Instantiates a new Trade.
     *
     * @param id        the id
     * @param account   the account
     * @param type      the type
     * @param tradeDate the trade date
     */
    public Trade(final int id, final String account, final String type, final Instant tradeDate) {
        this.account = account;
        this.type = type;
        this.id = id;
        this.tradeDate = tradeDate;
    }

    /**
     * Instantiates a new Trade.
     *
     * @param id           the id
     * @param account      the account
     * @param type         the type
     * @param buyQuantity  the buy quantity
     * @param sellQuantity the sell quantity
     * @param buyPrice     the buy price
     * @param sellPrice    the sell price
     * @param tradeDate    the trade date
     */
    public Trade(int id, final String account, final String type, final double buyQuantity, final double sellQuantity,
                 final double buyPrice, final double sellPrice, final Instant tradeDate) {
        this.id = id;
        this.account = account;
        this.type = type;
        this.buyQuantity = buyQuantity;
        this.sellQuantity = sellQuantity;
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
        this.tradeDate = tradeDate;
    }

    /**
     * Equals boolean.
     *
     * @param o the o
     * @return the boolean
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null) {
            Hibernate.getClass(this);
            Hibernate.getClass(o);
        }
        return false;
    }

    /**
     * Hash code int.
     *
     * @return the int
     */
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
