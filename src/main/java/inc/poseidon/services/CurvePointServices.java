package inc.poseidon.services;

import inc.poseidon.domain.CurvePoint;
import inc.poseidon.domain.CurvePointDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.mappers.ICurvePointDTOToCurvePointMapper;
import inc.poseidon.repositories.CurvePointRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Curve point services.
 */
@Service
public class CurvePointServices {

    /**
     * The Curve point repository.
     */
    private final CurvePointRepository curvePointRepository;

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CurvePointServices.class);

    /**
     * The Message.
     */
    private String message;

    /**
     * Instantiates a new Curve point services.
     *
     * @param curvePointRepository the curve point repository
     */
    public CurvePointServices(final CurvePointRepository curvePointRepository) {
        this.curvePointRepository = curvePointRepository;
    }

    /**
     * Format textMessage string.
     *
     * @param textMessage the textMessage
     * @param value   the value
     * @return the string
     */
    private String formatMessage(final String textMessage, final Integer value) {
        return String.format("%s %d", textMessage, value);
    }

    /**
     * Gets curve point list.
     *
     * @return the curve point list
     */
    @Transactional
    public List<CurvePointDTO> getCurvePointList() {
        LOGGER.debug("Beginning gathering all CurvePoints as a List");
        List<CurvePoint> curvePointList = curvePointRepository.findAll();
        List<CurvePointDTO> curvePointDTOList = new ArrayList<>();
        for (CurvePoint curvePoint : curvePointList) {
            curvePointDTOList.add(ICurvePointDTOToCurvePointMapper.INSTANCE.destinationToSource(curvePoint));
        }
        return curvePointDTOList;
    }

    /**
     * Gets curve point.
     *
     * @param curvePointId the curve point id
     * @return the curve point
     */
    @Transactional
    public CurvePointDTO getCurvePoint(final int curvePointId) {
        CurvePointDTO gatheredCurvePointDTO;
        try {
            CurvePoint curvePointRepositoryById = curvePointRepository.getById(curvePointId);
            gatheredCurvePointDTO = ICurvePointDTOToCurvePointMapper.INSTANCE.destinationToSource(curvePointRepositoryById);
        } catch (Exception e) {
            message = formatMessage("Error while getting curve point #", curvePointId);
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return gatheredCurvePointDTO;
    }

    /**
     * Gets next id.
     *
     * @return the next id
     */
    @Transactional
    public int getNextId() {
        return curvePointRepository.findAll().size();
    }


    /**
     * Add curve point curve point dto.
     *
     * @param curvePointDTO the curve point dto
     * @return the curve point dto
     * @throws PoseidonRuntimeException the poseidon runtime exception
     */
    @Transactional
    public CurvePointDTO addCurvePoint(final CurvePointDTO curvePointDTO) throws PoseidonRuntimeException {
        CurvePointDTO addedDTO;
        try {
            LOGGER.debug("Started to add a CurvePoint in the database");
            CurvePoint curvePoint = ICurvePointDTOToCurvePointMapper.INSTANCE.sourceToDestination(curvePointDTO);
            curvePoint.setId(getNextId());
            CurvePoint curvePointSaved = curvePointRepository.save(curvePoint);
            addedDTO = ICurvePointDTOToCurvePointMapper.INSTANCE.destinationToSource(curvePointSaved);
        } catch (Exception e) {
            message = formatMessage("Error while adding curve point with curvePointId #", curvePointDTO.getId());
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return addedDTO;
    }

    /**
     * Update curve point curve point dto.
     *
     * @param curvePointDTO the curve point dto
     * @return the curve point dto
     * @throws PoseidonRuntimeException the poseidon runtime exception
     */
    @Transactional
    public CurvePointDTO updateCurvePoint(final CurvePointDTO curvePointDTO) throws PoseidonRuntimeException {
        CurvePointDTO result;
        try {
            LOGGER.debug("Started to update Curve Point  #{} to database", curvePointDTO.getId());
            CurvePoint curvePoint = ICurvePointDTOToCurvePointMapper.INSTANCE.sourceToDestination(curvePointDTO);
            CurvePoint curvePointResult = curvePointRepository.save(curvePoint);
            result = ICurvePointDTOToCurvePointMapper.INSTANCE.destinationToSource(curvePointResult);
        } catch (Exception e) {
            message = formatMessage("error while updating curve point id #", curvePointDTO.getId());
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return result;
    }

    /**
     * Delete curve point void.
     *
     * @param curvePointId the curve point id
     * @return the void
     */
    @Transactional
    public Void deleteCurvePoint(final Integer curvePointId) {
        Void response = null;
        try {
            LOGGER.debug("Started to delete CurvePoint  #{} to database", curvePointId);
            curvePointRepository.delete(curvePointRepository.getById(curvePointId));

        } catch (Exception e) {
            message = formatMessage("error while deleting curve point id #", curvePointId);
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return response;
    }
}
