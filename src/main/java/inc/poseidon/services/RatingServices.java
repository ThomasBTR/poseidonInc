package inc.poseidon.services;

import inc.poseidon.domain.Rating;
import inc.poseidon.domain.RatingDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.mappers.IRatingToDTOMapper;
import inc.poseidon.repositories.RatingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Rating services.
 */
@Service
public class RatingServices {

    /**
     * The Rating repository.
     */
    private final RatingRepository ratingRepository;

    /**
     * The Message.
     */
    private String message;


    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(RatingServices.class);

    /**
     * Instantiates a new Rating services.
     *
     * @param ratingRepository the rating repository
     */
    public RatingServices(final RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    /**
     * Format message string.
     *
     * @param textMessage the message
     * @param value       the value
     * @return the string
     */
    private String formatMessage(final String textMessage, final Integer value) {
        return String.format("%s %d", textMessage, value);
    }


    /**
     * Gets rating list.
     *
     * @return the rating list
     */
    @Transactional
    public List<RatingDTO> getRatingList() {
        LOGGER.debug("Beginning gathering all Rating as a List");
        List<Rating> ratingList = ratingRepository.findAll();
        List<RatingDTO> ratingDTOList = new ArrayList<>();
        for (Rating rating : ratingList) {
            ratingDTOList.add(IRatingToDTOMapper.INSTANCE.destinationToSource(rating));
        }
        return ratingDTOList;
    }

    /**
     * Gets rating.
     *
     * @param ratingId the rating id
     * @return the rating
     */
    @Transactional
    public RatingDTO getRating(final int ratingId) {
        RatingDTO gatheredRatingDTO;
        try {
            Rating ratingRepositoryById = ratingRepository.getById(ratingId);
            gatheredRatingDTO = IRatingToDTOMapper.INSTANCE.destinationToSource(ratingRepositoryById);
        } catch (Exception e) {
            message = formatMessage("Error while getting rating #", ratingId);
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return gatheredRatingDTO;
    }


    /**
     * Gets next id.
     *
     * @return the next id
     */
    @Transactional
    public int getNextId() {
        return ratingRepository.findAll().size();
    }


    /**
     * Add rating rating dto.
     *
     * @param ratingDTO the rating dto
     * @return the rating dto
     * @throws PoseidonRuntimeException the poseidon runtime exception
     */
    @Transactional
    public RatingDTO addRating(final RatingDTO ratingDTO) throws PoseidonRuntimeException {
        RatingDTO addedDTO;
        try {
            LOGGER.debug("Started to add a Rating in the database");
            Rating rating = IRatingToDTOMapper.INSTANCE.sourceToDestination(ratingDTO);
            rating.setId(getNextId());
            Rating savedRating = ratingRepository.save(rating);
            addedDTO = IRatingToDTOMapper.INSTANCE.destinationToSource(savedRating);
        } catch (Exception e) {
            message = formatMessage("Error while adding rating with id #", ratingDTO.getId());
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return addedDTO;
    }


    /**
     * Update rating rating dto.
     *
     * @param ratingDTO the rating dto
     * @return the rating dto
     * @throws PoseidonRuntimeException the poseidon runtime exception
     */
    @Transactional
    public RatingDTO updateRating(final RatingDTO ratingDTO) throws PoseidonRuntimeException {
        RatingDTO result;
        try {
            LOGGER.debug("Started to update rating  #{} to database", ratingDTO.getId());
            Rating rating = IRatingToDTOMapper.INSTANCE.sourceToDestination(ratingDTO);
            Rating savedRating = ratingRepository.save(rating);
            result = IRatingToDTOMapper.INSTANCE.destinationToSource(savedRating);
        } catch (Exception e) {
            message = formatMessage("error while updating rating with id #", ratingDTO.getId());
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return result;
    }

    /**
     * Delete rating void.
     *
     * @param curvePointId the curve point id
     * @return the void
     */
    @Transactional
    public Void deleteRating(final Integer curvePointId) {
        Void returned = null;
        try {
            LOGGER.debug("Started to delete rating  #{} to database", curvePointId);
            ratingRepository.delete(ratingRepository.getById(curvePointId));

        } catch (Exception e) {
            message = formatMessage("error while deleting rating with id #", curvePointId);
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return returned;
    }
}
