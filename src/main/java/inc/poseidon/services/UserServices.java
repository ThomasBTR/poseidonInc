package inc.poseidon.services;

import inc.poseidon.domain.User;
import inc.poseidon.domain.UserDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.mappers.IUserToDTOMapper;
import inc.poseidon.repositories.UserRepository;
import inc.poseidon.securities.SpringSecurityConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Rating services.
 */
@Service
public class UserServices {


    /**
     * The User repository.
     */
    private final UserRepository userRepository;

    private final SpringSecurityConfig springSecurityConfig;

    /**
     * The Message.
     */
    private String message;


    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServices.class);


    /**
     * Instantiates a new User services.
     *
     * @param userRepository       the user repository
     * @param springSecurityConfig the spring security config
     */
    public UserServices(final UserRepository userRepository, SpringSecurityConfig springSecurityConfig) {
        this.userRepository = userRepository;
        this.springSecurityConfig = springSecurityConfig;
    }

    /**
     * Format message string.
     *
     * @param textMessage the message
     * @param value       the value
     * @return the string
     */
    private String formatMessage(final String textMessage, final Integer value) {
        return String.format("%s %d", textMessage, value);
    }


    /**
     * Gets user list.
     *
     * @return the user list
     */
    @Transactional
    public List<UserDTO> getUserList() {
        LOGGER.debug("Beginning gathering all users as a List");
        List<User> userList = userRepository.findAll();
        List<UserDTO> userDTOList = new ArrayList<>();
        for (User trade : userList) {
            userDTOList.add(IUserToDTOMapper.INSTANCE.destinationToSource(trade));
        }
        return userDTOList;
    }


    /**
     * Gets user.
     *
     * @param userId the user id
     * @return the user
     */
    @Transactional
    public UserDTO getUser(final int userId) {
        UserDTO gatheredUserDTO;
        try {
            User userRepositoryById = userRepository.getById(userId);
            gatheredUserDTO = IUserToDTOMapper.INSTANCE.destinationToSource(userRepositoryById);
        } catch (Exception e) {
            message = formatMessage("Error while getting user with id #", userId);
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return gatheredUserDTO;
    }


    /**
     * Gets next id.
     *
     * @return the next id
     */
    @Transactional
    public int getNextId() {
        return userRepository.findAll().size();
    }


    /**
     * Create user user.
     *
     * @param username the username
     * @param password the password
     */
    @Transactional
    public void createUser(final String username, final String fullName, final String password) {
        User user;
        List<User> verify = userRepository.findAll();
        if (verify.stream().anyMatch(user1 -> user1.getUsername().equals(username))) {
            LOGGER.warn("user already in database with username {}", username);
            //throw exception.
        } else {
            user = new User(getNextId(), username, fullName, springSecurityConfig.passwordEncoder().encode(password));
            userRepository.save(user);
        }
    }

    /**
     * Add user user dto.
     *
     * @param userDTO the user dto
     * @return the user dto
     * @throws PoseidonRuntimeException the poseidon runtime exception
     */
    @Transactional
    public UserDTO addUser(final UserDTO userDTO) throws PoseidonRuntimeException {
        UserDTO addedUserDTO;
        try {
            LOGGER.debug("Started to add a user in the database");
            User user = IUserToDTOMapper.INSTANCE.sourceToDestination(userDTO);
            user.setId(getNextId());
            User savedUser = userRepository.save(user);
            addedUserDTO = IUserToDTOMapper.INSTANCE.destinationToSource(savedUser);
        } catch (Exception e) {
            message = formatMessage("Error while adding user with id #", userDTO.getId());
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return addedUserDTO;
    }


    /**
     * Update user dto.
     *
     * @param userDTO the user dto
     * @return the user dto
     * @throws PoseidonRuntimeException the poseidon runtime exception
     */
    @Transactional
    public UserDTO updateUser(final UserDTO userDTO) throws PoseidonRuntimeException {
        UserDTO result;
        try {
            LOGGER.debug("Started to update user with id #{} to database", userDTO.getId());
            User user = IUserToDTOMapper.INSTANCE.sourceToDestination(userDTO);
            User savedUser = userRepository.save(user);
            result = IUserToDTOMapper.INSTANCE.destinationToSource(savedUser);
        } catch (Exception e) {
            message = formatMessage("error while updating user with id #", userDTO.getId());
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return result;
    }


    /**
     * Delete user void.
     *
     * @param userId the user id
     * @return the void
     */
    @Transactional
    public Void deleteUser(final Integer userId) {
        Void returned = null;
        try {
            LOGGER.debug("Started to delete user with id #{} to database", userId);
            userRepository.delete(userRepository.getById(userId));

        } catch (Exception e) {
            message = formatMessage("error while deleting user with id #", userId);
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return returned;
    }
}
