package inc.poseidon.services;

import inc.poseidon.domain.Bid;
import inc.poseidon.domain.BidDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.mappers.IBidDTOToBidMapper;
import inc.poseidon.repositories.BidListRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Bid services.
 */
@Service
public class BidServices {

    /**
     * The Bid list repository.
     */
    private final BidListRepository bidListRepository;

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(BidServices.class);

    /**
     * The Message.
     */
    private String message;

    /**
     * Instantiates a new Bid services.
     *
     * @param bidListRepository the bid list repository
     */
    public BidServices(final BidListRepository bidListRepository) {
        this.bidListRepository = bidListRepository;
    }

    /**
     * Format textMessage string.
     *
     * @param textMessage the textMessage
     * @param value   the value
     * @return the string
     */
    private String formatMessage(final String textMessage, final Integer value) {
        return String.format("%s %d", textMessage, value);
    }

    /**
     * Gets all bid lists.
     *
     * @return the all bid lists
     */
    @Transactional
    public List<BidDTO> getAllBidLists() {
        LOGGER.debug("Begining gathering all bid lists");
        List<Bid> bids = bidListRepository.findAll();
        List<BidDTO> bidDTOS = new ArrayList<>();
        for (Bid bid : bids) {
            bidDTOS.add(IBidDTOToBidMapper.INSTANCE.destinationToSource(bid));
        }
        return bidDTOS;
    }

    /**
     * Gets bid list.
     *
     * @param bidListId the bid list id
     * @return the bid list
     */
    @Transactional
    public BidDTO getBidList(final int bidListId) {
        BidDTO gatheredBidDTO;
        try {
            Bid bid = bidListRepository.getById(bidListId);
            gatheredBidDTO = IBidDTOToBidMapper.INSTANCE.destinationToSource(bid);
        } catch (Exception e) {
            message = formatMessage("Error while getting bid List #", bidListId);
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return gatheredBidDTO;
    }

    /**
     * Gets next id.
     *
     * @return the next id
     */
    @Transactional
    public int getNextId() {
        return bidListRepository.findAll().size();
    }


    /**
     * Add bid list to list bid dto.
     *
     * @param bidDTO the bid dto
     * @return the bid dto
     * @throws PoseidonRuntimeException the poseidon runtime exception
     */
    @Transactional
    public BidDTO addBidListToList(final BidDTO bidDTO) throws PoseidonRuntimeException {
        BidDTO bidListAdded;
        try {
            LOGGER.debug("Started to add BidList to database");
            Bid bid = IBidDTOToBidMapper.INSTANCE.sourceToDestination(bidDTO);
            bid.setBidId(getNextId());
            Bid bidResult = bidListRepository.save(bid);
            bidListAdded = IBidDTOToBidMapper.INSTANCE.destinationToSource(bidResult);
        } catch (Exception e) {
            message = String.format("%s %s", "Error while adding bid List with account field", bidDTO.getAccount());
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return bidListAdded;
    }

    /**
     * Update bid list bid dto.
     *
     * @param bidDTO the bid dto
     * @return the bid dto
     * @throws PoseidonRuntimeException the poseidon runtime exception
     */
    @Transactional
    public BidDTO updateBidList(final BidDTO bidDTO) throws PoseidonRuntimeException {
        BidDTO result;
        try {
            LOGGER.debug("Started to update BidList  #{} to database", bidDTO.getId());
            Bid bid = IBidDTOToBidMapper.INSTANCE.sourceToDestination(bidDTO);
            Bid bidResult = bidListRepository.save(bid);
            result = IBidDTOToBidMapper.INSTANCE.destinationToSource(bidResult);
        } catch (Exception e) {
            message = formatMessage("error while updating bid list #", bidDTO.getId());
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }

        return result;
    }

    /**
     * Delete bid list void.
     *
     * @param bidListId the bid list id
     * @return the void
     */
    @Transactional
    public Void deleteBidList(final Integer bidListId) {
        Void response = null;
        try {
            LOGGER.debug("Started to update BidList  #{} to database", bidListId);
            bidListRepository.delete(bidListRepository.getById(bidListId));

        } catch (Exception e) {
            message = formatMessage("error while deleting bid list #", bidListId);
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return response;
    }
}
