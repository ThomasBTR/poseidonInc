package inc.poseidon.services;

import inc.poseidon.domain.Trade;
import inc.poseidon.domain.TradeDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.mappers.ITradeToDTOMapper;
import inc.poseidon.repositories.TradeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Rating services.
 */
@Service
public class TradeServices {


    /**
     * The Trade repository.
     */
    private final TradeRepository tradeRepository;

    /**
     * The Message.
     */
    private String message;


    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(TradeServices.class);

    /**
     * Instantiates a new Trade services.
     *
     * @param tradeRepository the trade repository
     */
    public TradeServices(final TradeRepository tradeRepository) {
        this.tradeRepository = tradeRepository;
    }

    /**
     * Format message string.
     *
     * @param textMessage the message
     * @param value       the value
     * @return the string
     */
    private String formatMessage(final String textMessage, final Integer value) {
        return String.format("%s %d", textMessage, value);
    }


    /**
     * Gets trade list.
     *
     * @return the trade list
     */
    @Transactional
    public List<TradeDTO> getTradeList() {
        LOGGER.debug("Beginning gathering all Trade as a List");
        List<Trade> tradeList = tradeRepository.findAll();
        List<TradeDTO> tradeDTOList = new ArrayList<>();
        for (Trade trade : tradeList) {
            tradeDTOList.add(ITradeToDTOMapper.INSTANCE.destinationToSource(trade));
        }
        return tradeDTOList;
    }


    /**
     * Gets trade.
     *
     * @param tradeId the trade id
     * @return the trade
     */
    @Transactional
    public TradeDTO getTrade(final int tradeId) {
        TradeDTO gatheredTradeDTO;
        try {
            Trade tradeRepositoryById = tradeRepository.getById(tradeId);
            gatheredTradeDTO = ITradeToDTOMapper.INSTANCE.destinationToSource(tradeRepositoryById);
        } catch (Exception e) {
            message = formatMessage("Error while getting trade with id #", tradeId);
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return gatheredTradeDTO;
    }


    /**
     * Gets next id.
     *
     * @return the next id
     */
    @Transactional
    public int getNextId() {
        return tradeRepository.findAll().size();
    }


    /**
     * Add trade trade dto.
     *
     * @param tradeDTO the trade dto
     * @return the trade dto
     * @throws PoseidonRuntimeException the poseidon runtime exception
     */
    @Transactional
    public TradeDTO addTrade(final TradeDTO tradeDTO) throws PoseidonRuntimeException {
        TradeDTO addedDTO;
        try {
            LOGGER.debug("Started to add a trade in the database");
            Trade trade = ITradeToDTOMapper.INSTANCE.sourceToDestination(tradeDTO);
            trade.setId(getNextId());
            Trade savedTrade = tradeRepository.save(trade);
            addedDTO = ITradeToDTOMapper.INSTANCE.destinationToSource(savedTrade);
        } catch (Exception e) {
            message = formatMessage("Error while adding trade with id #", tradeDTO.getId());
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return addedDTO;
    }


    /**
     * Update trade trade dto.
     *
     * @param tradeDTO the trade dto
     * @return the trade dto
     * @throws PoseidonRuntimeException the poseidon runtime exception
     */
    @Transactional
    public TradeDTO updateTrade(final TradeDTO tradeDTO) throws PoseidonRuntimeException {
        TradeDTO result;
        try {
            LOGGER.debug("Started to update trade with id #{} to database", tradeDTO.getId());
            Trade trade = ITradeToDTOMapper.INSTANCE.sourceToDestination(tradeDTO);
            Trade savedTrade = tradeRepository.save(trade);
            result = ITradeToDTOMapper.INSTANCE.destinationToSource(savedTrade);
        } catch (Exception e) {
            message = formatMessage("error while updating trade with id #", tradeDTO.getId());
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return result;
    }


    /**
     * Delete trade void.
     *
     * @param tradeId the trade id
     * @return the void
     */
    @Transactional
    public Void deleteTrade(final Integer tradeId) {
        Void returned = null;
        try {
            LOGGER.debug("Started to delete trade with id #{} to database", tradeId);
            tradeRepository.delete(tradeRepository.getById(tradeId));

        } catch (Exception e) {
            message = formatMessage("error while deleting trade with id #", tradeId);
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return returned;
    }
}
