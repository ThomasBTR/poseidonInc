package inc.poseidon.services;

import inc.poseidon.domain.RuleName;
import inc.poseidon.domain.RuleNameDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.mappers.IRuleNameToDTOMapper;
import inc.poseidon.repositories.RuleNameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Rating services.
 */
@Service
public class RuleNameServices {


    /**
     * The Rule name repository.
     */
    private final RuleNameRepository ruleNameRepository;

    /**
     * The Message.
     */
    private String message;


    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(RuleNameServices.class);


    /**
     * Instantiates a new Rule name services.
     *
     * @param ruleNameRepository the rule name repository
     */
    public RuleNameServices(final RuleNameRepository ruleNameRepository) {
        this.ruleNameRepository = ruleNameRepository;
    }

    /**
     * Format message string.
     *
     * @param textMessage the message
     * @param value       the value
     * @return the string
     */
    private String formatMessage(final String textMessage, final Integer value) {
        return String.format("%s %d", textMessage, value);
    }


    /**
     * Gets rule name list.
     *
     * @return the rule name list
     */
    @Transactional
    public List<RuleNameDTO> getRuleNameList() {
        LOGGER.debug("Beginning gathering all Rating as a List");
        List<RuleName> ruleNameList = ruleNameRepository.findAll();
        List<RuleNameDTO> ruleNameDTOList = new ArrayList<>();
        for (RuleName ruleName : ruleNameList) {
            ruleNameDTOList.add(IRuleNameToDTOMapper.INSTANCE.destinationToSource(ruleName));
        }
        return ruleNameDTOList;
    }


    /**
     * Gets rule name.
     *
     * @param ratingId the rating id
     * @return the rule name
     */
    @Transactional
    public RuleNameDTO getRuleName(final int ratingId) {
        RuleNameDTO gatheredRuleNameDTO;
        try {
            RuleName ruleNameRepositoryById = ruleNameRepository.getById(ratingId);
            gatheredRuleNameDTO = IRuleNameToDTOMapper.INSTANCE.destinationToSource(ruleNameRepositoryById);
        } catch (Exception e) {
            message = formatMessage("Error while getting ruleName with id #", ratingId);
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return gatheredRuleNameDTO;
    }


    /**
     * Gets next id.
     *
     * @return the next id
     */
    @Transactional
    public int getNextId() {
        return ruleNameRepository.findAll().size();
    }


    /**
     * Add rule name rule name dto.
     *
     * @param ruleNameDTO the rule name dto
     * @return the rule name dto
     * @throws PoseidonRuntimeException the poseidon runtime exception
     */
    @Transactional
    public RuleNameDTO addRuleName(final RuleNameDTO ruleNameDTO) throws PoseidonRuntimeException {
        RuleNameDTO addedDTO;
        try {
            LOGGER.debug("Started to add a RuleName in the database");
            RuleName ruleName = IRuleNameToDTOMapper.INSTANCE.sourceToDestination(ruleNameDTO);
            ruleName.setId(getNextId());
            RuleName savedRuleName = ruleNameRepository.save(ruleName);
            addedDTO = IRuleNameToDTOMapper.INSTANCE.destinationToSource(savedRuleName);
        } catch (Exception e) {
            message = formatMessage("Error while adding ruleName with id #", ruleNameDTO.getId());
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return addedDTO;
    }


    /**
     * Update rule name rule name dto.
     *
     * @param ruleNameDTO the rule name dto
     * @return the rule name dto
     * @throws PoseidonRuntimeException the poseidon runtime exception
     */
    @Transactional
    public RuleNameDTO updateRuleName(final RuleNameDTO ruleNameDTO) throws PoseidonRuntimeException {
        RuleNameDTO result;
        try {
            LOGGER.debug("Started to update rule name with id #{} to database", ruleNameDTO.getId());
            RuleName ruleName = IRuleNameToDTOMapper.INSTANCE.sourceToDestination(ruleNameDTO);
            RuleName savedRuleName = ruleNameRepository.save(ruleName);
            result = IRuleNameToDTOMapper.INSTANCE.destinationToSource(savedRuleName);
        } catch (Exception e) {
            message = formatMessage("error while updating ruleName with id #", ruleNameDTO.getId());
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return result;
    }

    /**
     * Delete rule name void.
     *
     * @param ruleNameId the rule name id
     * @return the void
     */
    @Transactional
    public Void deleteRuleName(final Integer ruleNameId) {
        Void returned = null;
        try {
            LOGGER.debug("Started to delete rule name with id #{} to database", ruleNameId);
            ruleNameRepository.delete(ruleNameRepository.getById(ruleNameId));

        } catch (Exception e) {
            message = formatMessage("error while deleting ruleName with id #", ruleNameId);
            LOGGER.error(message);
            throw new PoseidonRuntimeException(message, e);
        }
        return returned;
    }
}
