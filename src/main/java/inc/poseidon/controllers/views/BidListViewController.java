package inc.poseidon.controllers.views;

import inc.poseidon.controllers.rests.BidRestController;
import inc.poseidon.domain.BidDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;


/**
 * The type Bid list controller.
 */
@Controller
public class BidListViewController {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(BidListViewController.class);

    /**
     * The Bid rest controller.
     */
    private final BidRestController bidRestController;

    /**
     * Instantiates a new Bid list controller.
     *
     * @param bidRestController the bid list services
     */
    public BidListViewController(final BidRestController bidRestController) {
        this.bidRestController = bidRestController;
    }

    /**
     * Home string.
     *
     * @param model the model
     * @return the string
     */
    @GetMapping("/bids/list")
    public String bidLists(final Model model) {
        ResponseEntity<List<BidDTO>> bidLists = bidRestController.bidList();
        LOGGER.info("Bid list gathered from database");
        model.addAttribute("bidLists", bidLists.getBody());
        return "bids/list";
    }

    /**
     * Add bid form string.
     *
     * @param bidDTO the bid dto
     * @param model  the model
     * @return the string
     */
    @GetMapping("/bids/add")
    public String addBidForm(final BidDTO bidDTO, final Model model) {
        model.addAttribute("bidDTO", bidDTO);
        return "bids/add";
    }

    /**
     * Validate string.
     *
     * @param bidDTO        the bid dto
     * @param bindingResult the binding result
     * @param model         the model
     * @return the string
     */
    @PostMapping("/bids/validate")
    public String validate(@Valid @ModelAttribute("bidDTO") final BidDTO bidDTO, final BindingResult bindingResult, final Model model) {
        String validateBidResponse;
        //Return error if BidDTO contain invalid data
        if (bindingResult.hasErrors()) {
            validateBidResponse = "bids/add";
        } else {
            // Else save and return the bidList added
            ResponseEntity<BidDTO> responseEntity = bidRestController.bidAdd(bidDTO);
            LOGGER.info("BidList #{} added to the bidList table in database", Objects.requireNonNull(responseEntity.getBody()).getId());
            validateBidResponse = bidLists(model);
        }
        return validateBidResponse;
    }

    /**
     * Show update form string.
     *
     * @param id     the id
     * @param bidDTO
     * @param model  the model
     * @return the string
     */
    @GetMapping("/bids/update/{id}")
    public String showUpdateForm(@PathVariable("id") int id, @ModelAttribute("bidList") final BidDTO bidDTO, final Model model) {
        model.addAttribute("bidList", bidDTO);
        return "bids/update";
    }

    /**
     * Update bid string.
     *
     * @param id     the id
     * @param bidDTO the bid list dto
     * @param result the result
     * @param model  the model
     * @return the string
     */
    @PostMapping("/bids/update/{id}")
    public String updateBid(@PathVariable("id") final Integer id, @Valid @ModelAttribute("bidList") final BidDTO bidDTO,
                            final BindingResult result, final Model model) {
        String updateBidResponse;
        //Return error if BidDTO contain invalid data
        if (result.hasErrors()) {
            updateBidResponse = showUpdateForm(id, bidDTO, model);
        } else {
            // Else save and return the bidList added
            ResponseEntity<BidDTO> responseEntity = bidRestController.bidUpdate(bidDTO);
            LOGGER.info("BidList #{} updated to the bidList table in database", Objects.requireNonNull(responseEntity.getBody()).getId());
            updateBidResponse = bidLists(model);
        }
        return updateBidResponse;
    }

    /**
     * Delete bid string.
     *
     * @param id    the id
     * @param model the model
     * @return the string
     */
    @GetMapping("/bids/delete/{id}")
    public String deleteBid(@PathVariable("id") final Integer id, final Model model) {
        bidRestController.bidDelete(id);
        LOGGER.info("BidList #{} deleted to the bidList table in database", id);
        return bidLists(model);
    }
}
