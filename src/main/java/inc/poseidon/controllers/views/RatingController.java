package inc.poseidon.controllers.views;

import inc.poseidon.controllers.rests.RatingRestController;
import inc.poseidon.domain.RatingDTO;
import inc.poseidon.services.RatingServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * The type Rating controller.
 */
@Controller
public class RatingController {
    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(RatingController.class);

    /**
     * The Rating rest controller.
     */
    private final RatingRestController ratingRestController;

    /**
     * The Rating services.
     */
    private final RatingServices ratingServices;

    /**
     * Instantiates a new Rating controller.
     *
     * @param ratingRestController the rating rest controller
     * @param ratingServices       the rating services
     */
    public RatingController(final RatingRestController ratingRestController, final RatingServices ratingServices) {
        this.ratingRestController = ratingRestController;
        this.ratingServices = ratingServices;
    }


    /**
     * Rating list string.
     *
     * @param model the model
     * @return the string
     */
    @GetMapping("/ratings/list")
    public String ratingList(final Model model) {
        ResponseEntity<List<RatingDTO>> ratingList = ratingRestController.getRatingList();
        LOGGER.info("Curve point list gathered from database");
        model.addAttribute("ratingList", ratingList.getBody());
        return "ratings/list";
    }


    /**
     * Add rating form string.
     *
     * @param ratingDTO the rating dto
     * @param model     the model
     * @return the string
     */
    @GetMapping("/ratings/add")
    public String addRatingForm(final RatingDTO ratingDTO, final Model model) {
        int next = ratingServices.getNextId();
        ratingDTO.setId(next);
        ratingDTO.setOrderNumber(next);
        model.addAttribute("ratingDTO", ratingDTO);
        return "ratings/add";
    }


    /**
     * Validate string.
     *
     * @param ratingDTO     the rating dto
     * @param bindingResult the binding result
     * @param model         the model
     * @return the string
     */
    @PostMapping("/ratings/validate")
    public String validate(@Valid final RatingDTO ratingDTO, final BindingResult bindingResult, final Model model) {
        String addRatingResponse;
        //Return error if CurvePointDTO contain invalid data
        if (bindingResult.hasErrors()) {
            addRatingResponse = "ratings/add";
        } else {
            // Else save and return the CurvePointDTO added
            ResponseEntity<RatingDTO> responseEntity = ratingRestController.ratingAdd(ratingDTO);
            LOGGER.info("Rating with id #{} added in database", Objects.requireNonNull(responseEntity.getBody()).getId());
            addRatingResponse = ratingList(model);
        }
        return addRatingResponse;
    }

    /**
     * Show update form string.
     *
     * @param id        the id
     * @param ratingDTO the rating dto
     * @param model     the model
     * @return the string
     */
    @GetMapping("/ratings/update/{id}")
    public String showUpdateForm(@PathVariable("id") final Integer id, @ModelAttribute("ratingDTO") final RatingDTO ratingDTO, final Model model) {
        model.addAttribute("ratingDTO", ratingDTO);
        return "ratings/update";
    }


    /**
     * Update rating string.
     *
     * @param id            the id
     * @param ratingDTO     the rating dto
     * @param bindingResult the binding result
     * @param model         the model
     * @return the string
     */
    @PostMapping("/ratings/update/{id}")
    public String updateRating(@PathVariable("id") final Integer id, @ModelAttribute("ratingDTO") @Valid final RatingDTO ratingDTO, final BindingResult bindingResult, final Model model) {
        String updatedRatingResponse;
        //Return error if curvePointDTO contain invalid data
        if (bindingResult.hasErrors()) {
            updatedRatingResponse = showUpdateForm(id, ratingDTO, model);
        } else {
            // Else save and return the curvePointDTO added
            ResponseEntity<RatingDTO> responseEntity = ratingRestController.ratingUpdate(ratingDTO);
            LOGGER.info("Rating with id #{} updated in database", Objects.requireNonNull(responseEntity.getBody()).getId());
            updatedRatingResponse = ratingList(model);
        }
        return updatedRatingResponse;
    }

    /**
     * Delete rating string.
     *
     * @param id    the id
     * @param model the model
     * @return the string
     */
    @GetMapping("/ratings/delete/{id}")
    public String deleteRating(@PathVariable("id") final Integer id, final Model model) {
        ratingRestController.ratingDelete(id);
        LOGGER.info("Rating with  id #{} deleted in database", id);
        return ratingList(model);
    }
}
