package inc.poseidon.controllers.views;

import inc.poseidon.controllers.rests.CurvePointRestController;
import inc.poseidon.domain.CurvePointDTO;
import inc.poseidon.services.CurvePointServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * The type Curve controller.
 */
@Controller
public class CurveController {
    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CurveController.class);

    /**
     * The Curve point rest controller.
     */
    private final CurvePointRestController curvePointRestController;

    /**
     * The Curve point services.
     */
    private final CurvePointServices curvePointServices;

    /**
     * Instantiates a new Curve controller.
     *
     * @param curvePointRestController the curve point rest controller
     * @param curvePointServices       the curve point services
     */
    public CurveController(final CurvePointRestController curvePointRestController, final CurvePointServices curvePointServices) {
        this.curvePointRestController = curvePointRestController;
        this.curvePointServices = curvePointServices;
    }


    /**
     * Curve point list string.
     *
     * @param model the model
     * @return the string
     */
    @GetMapping("/curvePoints/list")
    public String curvePointList(final Model model) {
        ResponseEntity<List<CurvePointDTO>> curvePointList = curvePointRestController.getCurvePointList();
        LOGGER.info("Curve point list gathered from database");
        model.addAttribute("curvePointList", curvePointList.getBody());
        return "curvePoints/list";
    }

    /**
     * Add CurvePoint form string.
     *
     * @param curvePointDTO the curve point dto
     * @param model         the model
     * @return the string
     */
    @GetMapping("/curvePoints/add")
    public String addCurvePointForm(final CurvePointDTO curvePointDTO, final Model model) {
        curvePointDTO.setId(curvePointServices.getNextId());
        model.addAttribute("curvePointDTO", curvePointDTO);
        return "curvePoints/add";
    }

    /**
     * Validate string.
     *
     * @param curvePointDTO the curve point
     * @param bindingResult the result
     * @param model         the model
     * @return the string
     */
    @PostMapping("/curvePoints/validate")
    public String validate(@Valid final CurvePointDTO curvePointDTO, final BindingResult bindingResult, final Model model) {
        String addCurvePointResponse;
        //Return error if CurvePointDTO contain invalid data
        if (bindingResult.hasErrors()) {
            addCurvePointResponse = "curvePoints/add";
        } else {
            // Else save and return the CurvePointDTO added
            ResponseEntity<CurvePointDTO> responseEntity = curvePointRestController.curvePointAdd(curvePointDTO);
            LOGGER.info("CurvePoint #{} added to the CurvePoint table in database", Objects.requireNonNull(responseEntity.getBody()).getId());
            addCurvePointResponse = curvePointList(model);
        }
        return addCurvePointResponse;
    }

    /**
     * Show update form string.
     *
     * @param id            the id
     * @param curvePointDTO the curve point dto
     * @param model         the model
     * @return the string
     */
    @GetMapping("/curvePoints/update/{id}")
    public String showUpdateForm(@PathVariable("id") final Integer id, @ModelAttribute("curvePointDTO") final CurvePointDTO curvePointDTO, final Model model) {
        model.addAttribute("curvePointDTO", curvePointDTO);
        return "curvePoints/update";
    }

    /**
     * Update CurvePoint string.
     *
     * @param id            the id
     * @param curvePointDTO the curve point
     * @param bindingResult the bindingResult
     * @param model         the model
     * @return the string
     */
    @PostMapping("/curvePoints/update/{id}")
    public String updateCurvePoint(@PathVariable("id") final Integer id, @ModelAttribute("curvePointDTO") @Valid final CurvePointDTO curvePointDTO,
                                   final BindingResult bindingResult, final Model model) {
        String updateCurvePointResponse;
        //Return error if curvePointDTO contain invalid data
        if (bindingResult.hasErrors()) {
            updateCurvePointResponse = showUpdateForm(id, curvePointDTO, model);
        } else {
            // Else save and return the curvePointDTO added
            ResponseEntity<CurvePointDTO> responseEntity = curvePointRestController.curvePointUpdate(curvePointDTO);
            LOGGER.info("CurvePoint #{} updated to the CurvePoint table in database", Objects.requireNonNull(responseEntity.getBody()).getId());
            updateCurvePointResponse = curvePointList(model);
        }
        return updateCurvePointResponse;
    }

    /**
     * Delete CurvePoint string.
     *
     * @param id    the id
     * @param model the model
     * @return the string
     */
    @GetMapping("/curvePoints/delete/{id}")
    public String deleteCurvePoint(@PathVariable("id") final Integer id, final Model model) {
        curvePointRestController.curvePointDelete(id);
        LOGGER.info("CurvePoint #{} deleted to the CurvePoint table in database", id);
        return curvePointList(model);
    }
}
