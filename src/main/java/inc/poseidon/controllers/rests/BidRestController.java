package inc.poseidon.controllers.rests;

import inc.poseidon.api.BidsApi;
import inc.poseidon.domain.BidDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.services.BidServices;
import io.swagger.v3.oas.annotations.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * The type Bid list rest controller.
 */
@RestController
public class BidRestController implements BidsApi {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(BidRestController.class);
    /**
     * The Bid services.
     */
    private final BidServices bidServices;


    /**
     * Instantiates a new Bid list rest controller.
     *
     * @param bidServices the bid list services
     */
    public BidRestController(final BidServices bidServices) {
        this.bidServices = bidServices;
    }

    /**
     * Bid list get all response entity.
     *
     * @return the response entity
     */
    @Override
    public ResponseEntity<List<BidDTO>> bidList() {
        ResponseEntity<List<BidDTO>> bidDTOs = null;
        try {
            bidDTOs = ResponseEntity.ok(bidServices.getAllBidLists());
            LOGGER.info("BidLists gathered from the database");
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while getting all bid lists from the database", e);
        }
        return bidDTOs;
    }

    /**
     * Bid list add response entity.
     *
     * @param bidDTO the bid dto
     * @return the response entity
     */
    @Override
    public ResponseEntity<BidDTO> bidAdd(
            @Parameter(name = "BidDTO", description = "bidList to add in the database") @Valid @RequestBody(required = false) final BidDTO bidDTO
    ) {
        ResponseEntity<BidDTO> responseEntity = null;
        try {
            responseEntity = ResponseEntity.ok(bidServices.addBidListToList(bidDTO));
            LOGGER.info("BidList #{} added to the bidList table in database", Objects.requireNonNull(responseEntity.getBody()).getId());
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while adding bid list to the database", e);
        }


        return responseEntity;
    }

    /**
     * Bid list update response entity.
     *
     * @param bidDTO the bid dto
     * @return the response entity
     */
    @Override
    public ResponseEntity<BidDTO> bidUpdate(
            @Parameter(name = "BidDTO", description = "bidList to update in the database", required = true) @Valid @RequestBody final BidDTO bidDTO
    ) {
        ResponseEntity<BidDTO> updatedBidList = null;
        try {
            updatedBidList = ResponseEntity.ok(bidServices.updateBidList(bidDTO));
            LOGGER.info("BidList #{} updated in the database", Objects.requireNonNull(updatedBidList.getBody()).getId());
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while updating bidList in the database", e);
        }
        return updatedBidList;
    }

    /**
     * Bid list delete response entity.
     *
     * @param bidListId the bid list id
     * @return the response entity
     */
    @Override
    public ResponseEntity<Void> bidDelete(
            @Parameter(name = "bidListId", description = "id of bidList", required = true) @PathVariable("bidListId") final Integer bidListId
    ) {
        try {
            bidServices.deleteBidList((bidListId));
            LOGGER.info("Bid #{} deleted in the database", Objects.requireNonNull(bidListId));
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while deleting bidList in the database", e);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
