package inc.poseidon.controllers.rests;


import inc.poseidon.api.RuleNamesApi;
import inc.poseidon.domain.RuleNameDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.services.RuleNameServices;
import io.swagger.v3.oas.annotations.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * The type Rating rest controller.
 */
@RestController
public class RuleNameRestController implements RuleNamesApi {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(RuleNameRestController.class);

    /**
     * The Rule name services.
     */
    private final RuleNameServices ruleNameServices;


    /**
     * Instantiates a new Rule name rest controller.
     *
     * @param ruleNameServices the rule name services
     */
    public RuleNameRestController(final RuleNameServices ruleNameServices) {
        this.ruleNameServices = ruleNameServices;
    }


    /**
     * Gets rule name list.
     *
     * @return the rule name list
     */
    @Override
    public ResponseEntity<List<RuleNameDTO>> getRuleNameList() {
        ResponseEntity<List<RuleNameDTO>> response = null;
        try {
            response = ResponseEntity.ok(ruleNameServices.getRuleNameList());
            LOGGER.info("RuleNameList gathered from the database");
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while getting RuleName List from the database", e);
        }
        return response;
    }


    /**
     * Rule name add response entity.
     *
     * @param ruleNameDTO the rule name dto
     * @return the response entity
     */
    @Override
    public ResponseEntity<RuleNameDTO> ruleNameAdd(
            @Parameter(name = "RuleNameDTO", description = "RuleName to add in the database", required = true) @Valid @RequestBody final RuleNameDTO ruleNameDTO
    ) {
        ResponseEntity<RuleNameDTO> responseEntity = null;
        try {
            responseEntity = ResponseEntity.ok(ruleNameServices.addRuleName(ruleNameDTO));
            LOGGER.info("RuleName with id #{} added to the database", Objects.requireNonNull(responseEntity.getBody()).getId());
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while adding ruleName to the database", e);
        }
        return responseEntity;
    }


    /**
     * Rule name update response entity.
     *
     * @param ruleNameDTO the rule name dto
     * @return the response entity
     */
    @Override
    public ResponseEntity<RuleNameDTO> ruleNameUpdate(
            @Parameter(name = "RuleNameDTO", description = "RuleName to update in the database", required = true) @Valid @RequestBody final RuleNameDTO ruleNameDTO
    ) {
        ResponseEntity<RuleNameDTO> updatedRuleName = null;
        try {
            updatedRuleName = ResponseEntity.ok(ruleNameServices.updateRuleName(ruleNameDTO));
            LOGGER.info("RuleName with id #{} updated in the database", Objects.requireNonNull(updatedRuleName.getBody()).getId());
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while updating RuleName in the database", e);
        }
        return updatedRuleName;
    }


    /**
     * Rule name delete response entity.
     *
     * @param ruleNameId the rule name id
     * @return the response entity
     */
    @Override
    public ResponseEntity<Void> ruleNameDelete(
            @Parameter(name = "ruleNameId", description = "id of RuleName", required = true) @PathVariable("ruleNameId") final Integer ruleNameId
    ) {
        try {
            ruleNameServices.deleteRuleName((ruleNameId));
            LOGGER.info("RuleName with id #{} deleted in the database", Objects.requireNonNull(ruleNameId));
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while deleting #{} RuleName in the database", ruleNameId, e);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
