package inc.poseidon.controllers.rests;


import inc.poseidon.api.RatingsApi;
import inc.poseidon.domain.RatingDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.services.RatingServices;
import io.swagger.v3.oas.annotations.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * The type Rating rest controller.
 */
@RestController
public class RatingRestController implements RatingsApi {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(RatingRestController.class);
    /**
     * The Rating services.
     */
    private final RatingServices ratingServices;


    /**
     * Instantiates a new Rating rest controller.
     *
     * @param ratingServices the rating services
     */
    public RatingRestController(final RatingServices ratingServices) {
        this.ratingServices = ratingServices;
    }

    /**
     * Gets rating list.
     *
     * @return the rating list
     */
    @Override
    public ResponseEntity<List<RatingDTO>> getRatingList() {
        ResponseEntity<List<RatingDTO>> response = null;
        try {
            response = ResponseEntity.ok(ratingServices.getRatingList());
            LOGGER.info("RatingList gathered from the database");
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while getting Rating List from the database", e);
        }
        return response;
    }

    /**
     * Rating add response entity.
     *
     * @param ratingDTO the rating dto
     * @return the response entity
     */
    @Override
    public ResponseEntity<RatingDTO> ratingAdd(
            @Parameter(name = "RatingDTO", description = "Rating to add in the database", required = true) @Valid @RequestBody final RatingDTO ratingDTO
    ) {
        ResponseEntity<RatingDTO> responseEntity = null;
        try {
            responseEntity = ResponseEntity.ok(ratingServices.addRating(ratingDTO));
            LOGGER.info("Rating #{} added to the database", Objects.requireNonNull(responseEntity.getBody()).getId());
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while adding rating to the database", e);
        }
        return responseEntity;
    }

    /**
     * Rating update response entity.
     *
     * @param ratingDTO the rating dto
     * @return the response entity
     */
    @Override
    public ResponseEntity<RatingDTO> ratingUpdate(
            @Parameter(name = "RatingDTO", description = "Rating to update in the database", required = true) @Valid @RequestBody final RatingDTO ratingDTO
    ) {
        ResponseEntity<RatingDTO> updatedRating = null;
        try {
            updatedRating = ResponseEntity.ok(ratingServices.updateRating(ratingDTO));
            LOGGER.info("Rating #{} updated in the database", Objects.requireNonNull(updatedRating.getBody()).getId());
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while updating Rating in the database", e);
        }
        return updatedRating;
    }

    /**
     * Rating delete response entity.
     *
     * @param ratingId the rating id
     * @return the response entity
     */
    @Override
    public ResponseEntity<Void> ratingDelete(
            @Parameter(name = "ratingId", description = "id of Rating", required = true) @PathVariable("ratingId") final Integer ratingId
    ) {
        try {
            ratingServices.deleteRating((ratingId));
            LOGGER.info("Rating #{} deleted in the database", Objects.requireNonNull(ratingId));
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while deleting #{} Rating in the database", ratingId, e);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
