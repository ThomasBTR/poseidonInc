package inc.poseidon.controllers.rests;


import inc.poseidon.api.TradesApi;
import inc.poseidon.domain.TradeDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.services.TradeServices;
import io.swagger.v3.oas.annotations.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * The type Rating rest controller.
 */
@RestController
public class TradeRestController implements TradesApi {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(TradeRestController.class);

    /**
     * The Trade services.
     */
    private final TradeServices tradeServices;


    /**
     * Instantiates a new Trade rest controller.
     *
     * @param tradeServices the trade services
     */
    public TradeRestController(final TradeServices tradeServices) {
        this.tradeServices = tradeServices;
    }


    /**
     * Gets trade list.
     *
     * @return the trade list
     */
    @Override
    public ResponseEntity<List<TradeDTO>> getTradeList() {
        ResponseEntity<List<TradeDTO>> response = null;
        try {
            response = ResponseEntity.ok(tradeServices.getTradeList());
            LOGGER.info("Trade List gathered from the database");
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while getting Trade List from the database", e);
        }
        return response;
    }


    /**
     * Rule name add response entity.
     *
     * @param tradeDTO the trade dto
     * @return the response entity
     */
    @Override
    public ResponseEntity<TradeDTO> tradeAdd(
            @Parameter(name = "TradeDTO", description = "Trade to add in the database", required = true) @Valid @RequestBody final TradeDTO tradeDTO
    ) {
        ResponseEntity<TradeDTO> responseEntity = null;
        try {
            responseEntity = ResponseEntity.ok(tradeServices.addTrade(tradeDTO));
            LOGGER.info("Trade with id #{} added to the database", Objects.requireNonNull(responseEntity.getBody()).getId());
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while adding Trade to the database", e);
        }
        return responseEntity;
    }


    /**
     * Trade update response entity.
     *
     * @param tradeDTO the trade dto
     * @return the response entity
     */
    @Override
    public ResponseEntity<TradeDTO> tradeUpdate(
            @Parameter(name = "TradeDTO", description = "Trade to update in the database", required = true) @Valid @RequestBody final TradeDTO tradeDTO
    ) {
        ResponseEntity<TradeDTO> updatedRuleName = null;
        try {
            updatedRuleName = ResponseEntity.ok(tradeServices.updateTrade(tradeDTO));
            LOGGER.info("Trade with id #{} updated in the database", Objects.requireNonNull(updatedRuleName.getBody()).getId());
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while updating Trade in the database", e);
        }
        return updatedRuleName;
    }


    /**
     * Trade delete response entity.
     *
     * @param tradeId the trade id
     * @return the response entity
     */
    @Override
    public ResponseEntity<Void> tradeDelete(
            @Parameter(name = "tradeId", description = "id of trade", required = true) @PathVariable("tradeId") final Integer tradeId
    ) {
        try {
            tradeServices.deleteTrade((tradeId));
            LOGGER.info("Trade with id #{} deleted in the database", Objects.requireNonNull(tradeId));
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while deleting #{} Trade in the database", tradeId, e);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
