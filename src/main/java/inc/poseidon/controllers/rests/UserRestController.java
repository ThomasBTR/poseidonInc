package inc.poseidon.controllers.rests;


import inc.poseidon.api.UsersApi;
import inc.poseidon.domain.UserDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.services.UserServices;
import io.swagger.v3.oas.annotations.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * The type Rating rest controller.
 */
@RestController
public class UserRestController implements UsersApi {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UserRestController.class);

    /**
     * The User services.
     */
    private final UserServices userServices;


    /**
     * Instantiates a new User rest controller.
     *
     * @param userServices the User services
     */
    public UserRestController(final UserServices userServices) {
        this.userServices = userServices;
    }


    /**
     * Gets User list.
     *
     * @return the User list
     */
    @Override
    public ResponseEntity<List<UserDTO>> getUserList() {
        ResponseEntity<List<UserDTO>> response = null;
        try {
            response = ResponseEntity.ok(userServices.getUserList());
            LOGGER.info("User List gathered from the database");
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while getting User List from the database", e);
        }
        return response;
    }


    /**
     * Rule name add response entity.
     *
     * @param userDTO the User dto
     * @return the response entity
     */
    @Override
    public ResponseEntity<UserDTO> userAdd(
            @Parameter(name = "UserDTO", description = "User to add in the database", required = true)
            @Valid @RequestBody final UserDTO userDTO) {
        ResponseEntity<UserDTO> responseEntity = null;
        try {
            responseEntity = ResponseEntity.ok(userServices.addUser(userDTO));
            LOGGER.info("User with id #{} added to the database", Objects.requireNonNull(responseEntity.getBody()).getId());
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while adding User to the database", e);
        }
        return responseEntity;
    }

    /**
     * User delete response entity.
     *
     * @param userId the User id
     * @return the response entity
     */
    @Override
    public ResponseEntity<Void> userDelete(
            @Parameter(name = "userId", description = "id of trade", required = true)
            @PathVariable("userId") final Integer userId) {
        try {
            userServices.deleteUser((userId));
            LOGGER.info("User with id #{} deleted in the database", Objects.requireNonNull(userId));
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while deleting #{} User in the database", userId, e);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * User update response entity.
     *
     * @param userDTO the User dto
     * @return the response entity
     */
    @Override
    public ResponseEntity<UserDTO> userUpdate(
            @Parameter(name = "UserDTO", description = "User to update in the database", required = true)
            @Valid @RequestBody final UserDTO userDTO) {
        ResponseEntity<UserDTO> updatedUser = null;
        try {
            updatedUser = ResponseEntity.ok(userServices.updateUser(userDTO));
            LOGGER.info("User with id #{} updated in the database", Objects.requireNonNull(updatedUser.getBody()).getId());
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while updating User in the database", e);
        }
        return updatedUser;
    }
}
