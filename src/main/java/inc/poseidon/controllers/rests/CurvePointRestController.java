package inc.poseidon.controllers.rests;



import inc.poseidon.api.CurvePointsApi;
import inc.poseidon.domain.CurvePointDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.services.CurvePointServices;
import io.swagger.v3.oas.annotations.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * The type Bid list rest controller.
 */
@RestController
public class CurvePointRestController implements CurvePointsApi {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CurvePointRestController.class);
    /**
     * The Curve point services.
     */
    private final CurvePointServices curvePointServices;


    /**
     * Instantiates a new Bid list rest controller.
     *
     * @param curvePointServices the bid list services
     */
    public CurvePointRestController(final CurvePointServices curvePointServices) {
        this.curvePointServices = curvePointServices;
    }

    /**
     * Gets curve point list.
     *
     * @return the curve point list
     */
    @Override
    public ResponseEntity<List<CurvePointDTO>> getCurvePointList() {
        ResponseEntity<List<CurvePointDTO>> response = null;
        try {
            response = ResponseEntity.ok(curvePointServices.getCurvePointList());
            LOGGER.info("CurvePointList gathered from the database");
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while getting curve point list from the database", e);
        }
        return response;
    }

    /**
     * Curve point add response entity.
     *
     * @param curvePointDTO the curve point dto
     * @return the response entity
     */
    @Override
    public ResponseEntity<CurvePointDTO> curvePointAdd(
            @Parameter(name = "CurvePointDTO", description = "CurvePoint to add in the database", required = true) @Valid @RequestBody final CurvePointDTO curvePointDTO
    ) {
        ResponseEntity<CurvePointDTO> responseEntity = null;
        try {
            responseEntity = ResponseEntity.ok(curvePointServices.addCurvePoint(curvePointDTO));
            LOGGER.info("CurvePoint #{} added to the curve point table in database", Objects.requireNonNull(responseEntity.getBody()).getId());
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while adding curve point to the database", e);
        }
        return responseEntity;
    }

    /**
     * Curve point update response entity.
     *
     * @param curvePointDTO the curve point dto
     * @return the response entity
     */
    @Override
    public ResponseEntity<CurvePointDTO> curvePointUpdate(
            @Parameter(name = "CurvePointDTO", description = "CurvePoint to update in the database", required = true) @Valid @RequestBody final CurvePointDTO curvePointDTO
    ) {
        ResponseEntity<CurvePointDTO> updatedCurvePoint = null;
        try {
            updatedCurvePoint = ResponseEntity.ok(curvePointServices.updateCurvePoint(curvePointDTO));
            LOGGER.info("Curve Point #{} updated in the database", Objects.requireNonNull(updatedCurvePoint.getBody()).getId());
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while updating Curve Point in the database", e);
        }
        return updatedCurvePoint;
    }

    /**
     * Curve point delete response entity.
     *
     * @param curvePointId the curve point id
     * @return the response entity
     */
    @Override
    public ResponseEntity<Void> curvePointDelete(
            @Parameter(name = "curvePointId", description = "id of CurvePoint", required = true) @PathVariable("curvePointId") final Integer curvePointId
    ) {
        try {
            curvePointServices.deleteCurvePoint((curvePointId));
            LOGGER.info("Curve Point #{} deleted in the database", Objects.requireNonNull(curvePointId));
        } catch (PoseidonRuntimeException e) {
            LOGGER.error("error while deleting Curve Point in the database", e);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
