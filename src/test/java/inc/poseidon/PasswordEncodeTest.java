package inc.poseidon;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Khang Nguyen.
 * Email: khang.nguyen@banvien.com
 * Date: 09/03/2019
 * Time: 11:26 AM
 */
@DataJpaTest
@RunWith(SpringRunner.class)
class PasswordEncodeTest {

    @Test
    void testPassword() {
        String rawPassword = "123456";
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String pw = encoder.encode(rawPassword);
        System.out.println("[ "+ pw + " ]");
        assertThat(encoder.matches(rawPassword, pw)).isTrue();
    }
}
