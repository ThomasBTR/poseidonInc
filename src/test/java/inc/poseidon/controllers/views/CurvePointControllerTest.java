package inc.poseidon.controllers.views;

import inc.poseidon.domain.CurvePointDTO;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class CurvePointControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @Order(1)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void curvePointList_OK() throws Exception {
        mockMvc.perform(get("/curvePoints/list"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("curvePointList", hasSize(5)));
    }

    @Test
    @Order(2)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void curvePointAdd_OK() throws Exception {
        mockMvc.perform(get("/curvePoints/add"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("curvePointDTO", instanceOf(CurvePointDTO.class)));
    }

    @Test
    @Order(3)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void validate_OK() throws Exception {
        CurvePointDTO curvePointDTO = new CurvePointDTO();
        curvePointDTO.setId(5);
        curvePointDTO.setTerm(500.05d);
        curvePointDTO.setValue(200d);
        mockMvc.perform(post("/curvePoints/validate")
                        .flashAttr("curvePointDTO", curvePointDTO)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(content().string(containsString("500.05")));
    }

    @Test
    @Order(3)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void update_OK() throws Exception {
        CurvePointDTO curvePointDTO = new CurvePointDTO();
        curvePointDTO.setId(0);
        curvePointDTO.setTerm(1150.05d);
        curvePointDTO.setValue(50.25d);
        mockMvc.perform(post("/curvePoints/update/{id}", "0")
                        .flashAttr("curvePointDTO", curvePointDTO)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(content().string(containsString("1150.05")))
                .andExpect(content().string(containsString("50.25")))
                .andExpect(view().name("curvePoints/list"));
    }

    @Test
    @Order(4)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void delete_OK() throws Exception {
        MvcResult result = mockMvc.perform(get("/curvePoints/delete/{id}", "0")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("curvePoints/list"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        //  Verify that roberto CurvePoint is deleted
        String content = result.getResponse().getContentAsString();
        assertThat(content).doesNotContain("100.0");
    }
}
