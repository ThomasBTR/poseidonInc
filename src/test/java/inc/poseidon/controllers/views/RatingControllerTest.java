package inc.poseidon.controllers.views;

import inc.poseidon.domain.RatingDTO;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * The type Rating controller test.
 */
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class RatingControllerTest {
    /**
     * The Mock mvc.
     */
    @Autowired
    private MockMvc mockMvc;

    /**
     * Rating list ok.
     *
     * @throws Exception the exception
     */
    @Test
    @Order(1)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void ratingList_OK() throws Exception {
        mockMvc.perform(get("/ratings/list"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("ratingList", hasSize(5)));
    }

    /**
     * Rating add ok.
     *
     * @throws Exception the exception
     */
    @Test
    @Order(2)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void ratingAdd_OK() throws Exception {
        mockMvc.perform(get("/ratings/add"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("ratingDTO", instanceOf(RatingDTO.class)));
    }

    /**
     * Rating validate ok.
     *
     * @throws Exception the exception
     */
    @Test
    @Order(3)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void ratingValidate_OK() throws Exception {
        RatingDTO ratingDTO = new RatingDTO();
        ratingDTO.setId(5);
        ratingDTO.setOrderNumber(5);
        ratingDTO.setMoodysRating("CCC");
        ratingDTO.setFitchRating("CCC");
        ratingDTO.setsAndPRating("CCC");
        mockMvc.perform(post("/ratings/validate")
                        .flashAttr("ratingDTO", ratingDTO)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(content().string(containsString("CCC")));
    }

    /**
     * Rating update ok.
     *
     * @throws Exception the exception
     */
    @Test
    @Order(4)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void ratingUpdate_OK() throws Exception {
        RatingDTO updatedDTO = new RatingDTO();
        updatedDTO.setId(5);
        updatedDTO.setOrderNumber(5);
        updatedDTO.setMoodysRating("AAA");
        updatedDTO.setFitchRating("CCC");
        updatedDTO.setsAndPRating("BBB");
        mockMvc.perform(post("/ratings/update/{id}", "0")
                        .flashAttr("ratingDTO", updatedDTO)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(content().string(containsString("AAA")))
                .andExpect(content().string(containsString("BBB")))
                .andExpect(content().string(containsString("CCC")))
                .andExpect(view().name("ratings/list"));
    }

    /**
     * Rating delete ok.
     *
     * @throws Exception the exception
     */
    @Test

    @Order(5)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void ratingDelete_OK() throws Exception {
        MvcResult result = mockMvc.perform(get("/ratings/delete/{id}", "0")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("ratings/list"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        //  Verify that roberto bidList is deleted
        String content = result.getResponse().getContentAsString();
        assertThat(content).doesNotContain("ABC");
    }
}
