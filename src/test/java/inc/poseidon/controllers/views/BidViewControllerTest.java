package inc.poseidon.controllers.views;

import inc.poseidon.domain.BidDTO;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class BidViewControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @Order(1)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void bidLists_OK() throws Exception {
        mockMvc.perform(get("/bids/list"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("bidLists", hasSize(5)));
    }

    @Test
    @Order(2)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void bidListAdd_OK() throws Exception {
        mockMvc.perform(get("/bids/add"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("bidDTO", instanceOf(BidDTO.class)));
    }

    @Test
    @Order(3)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void validate_OK() throws Exception {
        BidDTO BidDTO = new BidDTO();
        BidDTO.setType("type_test");
        BidDTO.setAccount("account_test");
        BidDTO.setBidQuantity(100d);
        BidDTO.setId(5);
        mockMvc.perform(post("/bids/validate")
                        .flashAttr("bidDTO", BidDTO)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(content().string(containsString("type_test")));
    }

    @Test
    @Order(4)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void update_OK() throws Exception {
        BidDTO bidDTO = new BidDTO();
        bidDTO.setId(0);
        bidDTO.setAccount("roberto");
        bidDTO.setBidQuantity(200d);
        bidDTO.setType("grade_Y");
        mockMvc.perform(post("/bids/update/{id}", "0")
                        .flashAttr("bidList", bidDTO)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(content().string(containsString("200.0")))
                .andExpect(content().string(containsString("grade_Y")))
                .andExpect(view().name("bids/list"));
    }

    @Test
    @Order(5)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void delete_OK() throws Exception {
        MvcResult result = mockMvc.perform(get("/bids/delete/{id}", "0")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("bids/list"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        //  Verify that roberto bidList is deleted
        String content = result.getResponse().getContentAsString();
        assertThat(content).doesNotContain("roberto");
    }
}
