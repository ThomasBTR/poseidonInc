package inc.poseidon.controllers.rests;


import com.fasterxml.jackson.databind.ObjectMapper;
import inc.poseidon.domain.CurvePointDTO;
import inc.poseidon.utils.UTHelper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * The type Curve point rest controller test.
 */
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class CurvePointRestControllerTest {

    /**
     * The Mock mvc.
     */
    @Autowired
    private MockMvc mockMvc;

    /**
     * Curve point list ok.
     *
     * @throws Exception the exception
     */
    @Order(1)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void curvePointList_OK() throws Exception {
        mockMvc.perform(get("/curvePoints")
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(content().json(UTHelper.readFileAsString("expected/curvepoints/curvepointList.json")));
    }

    /**
     * Validate ok.
     *
     * @throws Exception the exception
     */
    @Order(2)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void validate_OK() throws Exception {
        CurvePointDTO curvePointDTO = new CurvePointDTO();
        curvePointDTO.setValue(16d);
        curvePointDTO.setTerm(200d);
        curvePointDTO.setId(5);
        mockMvc.perform(post("/curvePoints")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(curvePointDTO))
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().json(UTHelper.readFileAsString("expected/curvepoints/curvepointValidate.json")));
    }

    /**
     * Update ok.
     *
     * @throws Exception the exception
     */
    @Order(3)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void update_OK() throws Exception {
        CurvePointDTO updatedCurvePointDTO = new CurvePointDTO();
        updatedCurvePointDTO.setValue(20d);
        updatedCurvePointDTO.setTerm(100d);
        updatedCurvePointDTO.setId(5);
        mockMvc.perform(put("/curvePoints")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(updatedCurvePointDTO))
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().json(UTHelper.readFileAsString("expected/curvepoints/curvepointUpdate.json")));
    }

    /**
     * Delete ok.
     *
     * @throws Exception the exception
     */
    @Order(4)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void delete_OK() throws Exception {
        mockMvc.perform(delete("/curvePoints/{curvePointId}", "0")
                        .with(csrf()))
                .andExpect(status().isNoContent());

    }

    /**
     * As json string string.
     *
     * @param obj the obj
     * @return the string
     */
    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
