package inc.poseidon.controllers.rests;


import com.fasterxml.jackson.databind.ObjectMapper;
import inc.poseidon.domain.RuleNameDTO;
import inc.poseidon.utils.UTHelper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * The type Rating rest controller test.
 */
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class RuleNameRestControllerTest {

    /**
     * The Mock mvc.
     */
    @Autowired
    private MockMvc mockMvc;

    /**
     * Rating list ok.
     *
     * @throws Exception the exception
     */
    @Order(1)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void ruleNameList_OK() throws Exception {
        mockMvc.perform(get("/ruleNames")
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(content().json(UTHelper.readFileAsString("expected/ruleNames/ruleNameList.json")));
    }

    /**
     * Validate ok.
     *
     * @throws Exception the exception
     */
    @Order(2)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void validate_OK() throws Exception {
        RuleNameDTO ruleNameDTO = new RuleNameDTO();
        ruleNameDTO.setName("Camille");
        ruleNameDTO.setDescription("Camille rule");
        ruleNameDTO.setId(5);
        mockMvc.perform(post("/ruleNames")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(ruleNameDTO))
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().json(UTHelper.readFileAsString("expected/ruleNames/ruleNameValidate.json")));
    }

    /**
     * Update ok.
     *
     * @throws Exception the exception
     */
    @Order(3)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void update_OK() throws Exception {
        RuleNameDTO updatedRuleNameDTO = new RuleNameDTO();
        updatedRuleNameDTO.setName("Camille");
        updatedRuleNameDTO.setDescription("Camille rule");
        updatedRuleNameDTO.setTemplate("Camillou");
        updatedRuleNameDTO.setId(5);
        mockMvc.perform(put("/ruleNames")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(updatedRuleNameDTO))
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().json(UTHelper.readFileAsString("expected/ruleNames/ruleNameUpdate.json")));
    }

    /**
     * Delete ok.
     *
     * @throws Exception the exception
     */
    @Order(4)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void delete_OK() throws Exception {
        mockMvc.perform(delete("/ruleNames/{ruleNameId}", "0")
                        .with(csrf()))
                .andExpect(status().isNoContent());

    }

    /**
     * As json string string.
     *
     * @param obj the obj
     * @return the string
     */
    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
