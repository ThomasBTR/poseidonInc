package inc.poseidon.controllers.rests;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import inc.poseidon.domain.TradeDTO;
import inc.poseidon.utils.JacksonConfiguration;
import inc.poseidon.utils.UTHelper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * The type Rating rest controller test.
 */
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class TradeRestControllerTest {

    /**
     * The Mock mvc.
     */
    @Autowired
    private MockMvc mockMvc;
    private static final JacksonConfiguration jacksonConfiguration = new JacksonConfiguration();

    /**
     * Rating list ok.
     *
     * @throws Exception the exception
     */
    @Order(1)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void tradeList_OK() throws Exception {
        String response = mockMvc.perform(get("/trades")
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andReturn().getResponse().getContentAsString();
//                .andExpect(content().json(UTHelper.readFileAsString("expected/trades/tradeList.json")));

//        assertEquals(UTHelper.stringToObject(response, List<TradeDTO>.class), UTHelper.stringToObject(UTHelper.readFileAsString("expected/trades/tradeList.json"), ArrayList.class));
        JSONAssert.assertEquals(UTHelper.readFileAsString("expected/trades/tradeList.json"), response, JSONCompareMode.LENIENT);
    }

    /**
     * Validate ok.
     *
     * @throws Exception the exception
     */
    @Order(2)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void validate_OK() throws Exception {
        TradeDTO tradeDTO = new TradeDTO();
        tradeDTO.setAccount("Camille");
        tradeDTO.setType("TypeC");
        Instant dateTime = Instant.parse("2022-06-13T11:50:39Z");
        tradeDTO.setTradeDate(dateTime);
        tradeDTO.setId(5);
        String response = mockMvc.perform(post("/trades")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(tradeDTO))
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        assertThat(getObject(TradeDTO.class, response)).isEqualTo(getObject(TradeDTO.class, UTHelper.readFileAsString("expected/trades/tradeValidate.json")));
    }

    /**
     * Update ok.
     *
     * @throws Exception the exception
     */
    @Order(3)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void update_OK() throws Exception {
        TradeDTO updatedTradeDTO = new TradeDTO();
        updatedTradeDTO.setAccount("Camille");
        updatedTradeDTO.setType("TypeC");
        updatedTradeDTO.setBuyQuantity(500d);
        updatedTradeDTO.setSellQuantity(400d);
        updatedTradeDTO.setBuyPrice(150d);
        updatedTradeDTO.setSellPrice(8000d);
        updatedTradeDTO.setId(5);
        Instant dateTime = Instant.parse("2022-06-13T11:50:39Z");
        updatedTradeDTO.setTradeDate(dateTime);
        String response = mockMvc.perform(put("/trades")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(updatedTradeDTO))
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertThat(UTHelper.stringToObject(response, TradeDTO.class)).isEqualTo(UTHelper.stringToObject(UTHelper.readFileAsString("expected/trades/tradeUpdate.json"), TradeDTO.class));
    }

    /**
     * Delete ok.
     *
     * @throws Exception the exception
     */
    @Order(4)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void delete_OK() throws Exception {
        mockMvc.perform(delete("/ruleNames/{ruleNameId}", "0")
                        .with(csrf()))
                .andExpect(status().isNoContent());

    }

    /**
     * As json string string.
     *
     * @param obj the obj
     * @return the string
     */
    public static String asJsonString(final Object obj) {
        try {
            ObjectMapper mapper = getObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Gets object mapper.
     *
     * @return the object mapper
     */
    public static ObjectMapper getObjectMapper() {
        return jacksonConfiguration.objectMapper();
    }

    /**
     * Gets object.
     *
     * @param className  the class name
     * @param jsonString the json string
     * @return the object
     * @throws JsonProcessingException the json processing exception
     */
    public static Object getObject(Class className, String jsonString) throws JsonProcessingException {
        ObjectMapper objectMapper = getObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(jsonString);

        return objectMapper.convertValue(jsonNode, className);
    }

}
