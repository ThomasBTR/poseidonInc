package inc.poseidon.controllers.rests;


import com.fasterxml.jackson.databind.ObjectMapper;
import inc.poseidon.domain.BidDTO;
import inc.poseidon.utils.UTHelper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * The type Bid rest controller test.
 */
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class BidRestControllerTest {

    /**
     * The Mock mvc.
     */
    @Autowired
    private MockMvc mockMvc;

    /**
     * Bid lists ok.
     *
     * @throws Exception the exception
     */
    @Test
    @Order(1)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void bidLists_OK() throws Exception {
        mockMvc.perform(get("/bids")
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(content().json(UTHelper.readFileAsString("expected/bids/bidLists.json")));
    }

    /**
     * Validate ok.
     *
     * @throws Exception the exception
     */
    @Test
    @Order(2)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void validate_OK() throws Exception {
        BidDTO BidDTO = new BidDTO();
        BidDTO.setType("type_test");
        BidDTO.setAccount("account_test");
        BidDTO.setBidQuantity(100d);
        BidDTO.setId(5);
        mockMvc.perform(post("/bids")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(BidDTO))
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().json(UTHelper.readFileAsString("expected/bids/bidListValidate.json")));
    }

    /**
     * Update ok.
     *
     * @throws Exception the exception
     */
    @Test
    @Order(3)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void update_OK() throws Exception {
        BidDTO BidDTO = new BidDTO();
        BidDTO.setId(0);
        BidDTO.setAccount("roberto");
        BidDTO.setBidQuantity(200d);
        BidDTO.setType("grade_Y");
        mockMvc.perform(put("/bids")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(BidDTO))
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().json(UTHelper.readFileAsString("expected/bids/bidListUpdate.json")));
    }

    /**
     * Delete ok.
     *
     * @throws Exception the exception
     */
    @Test
    @Order(3)
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void delete_OK() throws Exception {
        mockMvc.perform(delete("/bids/{bidListId}", "0")
                        .with(csrf()))
                .andExpect(status().isNoContent());

    }

    /**
     * As json string string.
     *
     * @param obj the obj
     * @return the string
     */
    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
