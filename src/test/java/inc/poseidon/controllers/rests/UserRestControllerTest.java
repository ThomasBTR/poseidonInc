package inc.poseidon.controllers.rests;


import com.fasterxml.jackson.databind.ObjectMapper;
import inc.poseidon.domain.UserDTO;
import inc.poseidon.utils.UTHelper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * The type Rating rest controller test.
 */
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class UserRestControllerTest {

    /**
     * The Mock mvc.
     */
    @Autowired
    private MockMvc mockMvc;

    /**
     * Rating list ok.
     *
     * @throws Exception the exception
     */
    @Order(1)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void userList_OK() throws Exception {
        mockMvc.perform(get("/users")
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(content().json(UTHelper.readFileAsString("expected/users/userList.json")));
    }

    /**
     * Validate ok.
     *
     * @throws Exception the exception
     */
    @Order(2)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void validate_OK() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setFullName("Camille");
        userDTO.setPassword("C@mille1998");
        userDTO.setId(2);
        userDTO.setUsername("Camcam");
        userDTO.setRole("ROLE_USER");
        mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userDTO))
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().json(UTHelper.readFileAsString("expected/users/userValidate.json")));
    }

    /**
     * Update ok.
     *
     * @throws Exception the exception
     */
    @Order(3)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void update_OK() throws Exception {
        UserDTO updatedUserDTO = new UserDTO();
        updatedUserDTO.setFullName("Camille");
        updatedUserDTO.setPassword("C@mille2022");
        updatedUserDTO.setId(2);
        updatedUserDTO.setUsername("Camcam22");
        updatedUserDTO.setRole("ROLE_USER");
        mockMvc.perform(put("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(updatedUserDTO))
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().json(UTHelper.readFileAsString("expected/users/userUpdate.json")));
    }

    /**
     * Delete ok.
     *
     * @throws Exception the exception
     */
    @Order(4)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void delete_OK() throws Exception {
        mockMvc.perform(delete("/users/{userId}", "0")
                        .with(csrf()))
                .andExpect(status().isNoContent());

    }

    /**
     * As json string string.
     *
     * @param obj the obj
     * @return the string
     */
    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
