package inc.poseidon.controllers.rests;


import com.fasterxml.jackson.databind.ObjectMapper;
import inc.poseidon.domain.RatingDTO;
import inc.poseidon.utils.UTHelper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * The type Rating rest controller test.
 */
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class RatingRestControllerTest {

    /**
     * The Mock mvc.
     */
    @Autowired
    private MockMvc mockMvc;

    /**
     * Rating list ok.
     *
     * @throws Exception the exception
     */
    @Order(1)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void ratingList_OK() throws Exception {
        mockMvc.perform(get("/ratings")
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(content().json(UTHelper.readFileAsString("expected/ratings/ratingList.json")));
    }

    /**
     * Validate ok.
     *
     * @throws Exception the exception
     */
    @Order(2)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void validate_OK() throws Exception {
        RatingDTO ratingDTO = new RatingDTO();
        ratingDTO.setsAndPRating("AA");
        ratingDTO.setFitchRating("AA");
        ratingDTO.setMoodysRating("BBB");
        ratingDTO.setOrderNumber(15);
        ratingDTO.setId(5);
        mockMvc.perform(post("/ratings")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(ratingDTO))
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().json(UTHelper.readFileAsString("expected/ratings/ratingValidate.json")));
    }

    /**
     * Update ok.
     *
     * @throws Exception the exception
     */
    @Order(3)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void update_OK() throws Exception {
        RatingDTO updatedRatingDTO = new RatingDTO();
        updatedRatingDTO.setsAndPRating("A");
        updatedRatingDTO.setFitchRating("BBB");
        updatedRatingDTO.setMoodysRating("C");
        updatedRatingDTO.setOrderNumber(15);
        updatedRatingDTO.setId(5);
        mockMvc.perform(put("/ratings")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(updatedRatingDTO))
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().json(UTHelper.readFileAsString("expected/ratings/ratingUpdate.json")));
    }

    /**
     * Delete ok.
     *
     * @throws Exception the exception
     */
    @Order(4)
    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void delete_OK() throws Exception {
        mockMvc.perform(delete("/ratings/{ratingId}", "0")
                        .with(csrf()))
                .andExpect(status().isNoContent());

    }

    /**
     * As json string string.
     *
     * @param obj the obj
     * @return the string
     */
    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
