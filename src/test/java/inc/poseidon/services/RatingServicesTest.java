package inc.poseidon.services;

import inc.poseidon.domain.Rating;
import inc.poseidon.domain.RatingDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.repositories.RatingRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class RatingServicesTest {


    @Autowired
    RatingServices ratingServices;
    @MockBean
    RatingRepository ratingRepository;

    Rating expectedRating;
    Rating updatedRating;

    RatingDTO expectedRatingDTO;

    RatingDTO updatedRatingDTO;


    @BeforeEach
    void prepare() {
        expectedRating =new Rating(1, "A", "A", "A", 1);
        updatedRating = new Rating(1, "B", "BB", "A", 1);
        expectedRatingDTO = new RatingDTO();
        expectedRatingDTO.setId(1);
        expectedRatingDTO.setMoodysRating("A");
        expectedRatingDTO.setsAndPRating("A");
        expectedRatingDTO.setFitchRating("A");
        expectedRatingDTO.setOrderNumber(1);

        updatedRatingDTO = new RatingDTO();
        updatedRatingDTO.setId(1);
        updatedRatingDTO.setMoodysRating("B");
        updatedRatingDTO.setsAndPRating("BB");
        updatedRatingDTO.setFitchRating("A");
        updatedRatingDTO.setOrderNumber(1);
    }

    @Test
    void getCurveRating_ShouldReturnOK() {
        //GIVEN
        List<Rating> ratingList = new ArrayList<>();
        ratingList.add(expectedRating);
        List<RatingDTO> ratingDTOList = new ArrayList<>();
        ratingDTOList.add(expectedRatingDTO);
        when(ratingRepository.findAll()).thenReturn(ratingList);
        //WHEN
        List<RatingDTO> actualRatingDTOList = ratingServices.getRatingList();
        //THEN
        assertThat(actualRatingDTOList).isEqualTo(ratingDTOList)
                .hasSize(1)
                .contains(expectedRatingDTO);
    }


    @Test
    void getRating_ShouldReturnOk() {
        //GIVEN
        when(ratingRepository.save(any(Rating.class))).thenReturn(expectedRating);
        when(ratingRepository.getById(1)).thenReturn(expectedRating);
        ratingServices.addRating(expectedRatingDTO);
        //WHEN
        RatingDTO gatheredRating = ratingServices.getRating(expectedRatingDTO.getId());
        assertThat(gatheredRating).isEqualTo(expectedRatingDTO);
    }

    @Test
    void getRating_ShouldReturnKO_IdNotInDatabase() {
        //GIVEN
        when(ratingRepository.save(any(Rating.class))).thenReturn(expectedRating);
        when(ratingRepository.getById(2)).thenThrow(new RuntimeException());
        ratingServices.addRating(expectedRatingDTO);
        //WHEN
        assertThatThrownBy(() -> ratingServices.getRating(2)).isInstanceOf(PoseidonRuntimeException.class)
                .hasMessage("Error while getting rating # 2");
    }

    @Test
    void add_shouldReturnRating() {
        //GIVEN
        when(ratingRepository.save(any(Rating.class))).thenReturn(expectedRating);
        //WHEN
        RatingDTO actualRatingDTO = ratingServices.addRating(expectedRatingDTO);
        //THEN
        assertThat(actualRatingDTO).isEqualTo(expectedRatingDTO);
        assertThat(actualRatingDTO.getId()).isEqualTo(expectedRatingDTO.getId());
        assertThat(actualRatingDTO.getOrderNumber()).isEqualTo(expectedRatingDTO.getOrderNumber());
        assertThat(actualRatingDTO.getFitchRating()).isEqualTo(expectedRatingDTO.getFitchRating());
        assertThat(actualRatingDTO.getMoodysRating()).isEqualTo(expectedRatingDTO.getMoodysRating());
        assertThat(actualRatingDTO.getsAndPRating()).isEqualTo(expectedRatingDTO.getsAndPRating());
    }


    @Test
    void addRating_shouldReturnError() {
        //GIVEN
        when(ratingRepository.save(any(Rating.class))).thenThrow(new RuntimeException());
        //WHEN
        assertThatThrownBy(() -> ratingServices.addRating(expectedRatingDTO))
                .isInstanceOf(PoseidonRuntimeException.class)
                .hasMessage("Error while adding rating with id # 1");
    }

    @Test
    void updateRating_ShouldReturnOk() {
        //GIVEN
        when(ratingRepository.save(any(Rating.class))).thenReturn(updatedRating);
        //WHEN
        RatingDTO actualRatingDTO = ratingServices.updateRating(updatedRatingDTO);
        //THEN
        assertThat(actualRatingDTO).isEqualTo(updatedRatingDTO);
        assertThat(actualRatingDTO.getId()).isEqualTo(updatedRatingDTO.getId());
        assertThat(actualRatingDTO.getOrderNumber()).isEqualTo(updatedRatingDTO.getOrderNumber());
        assertThat(actualRatingDTO.getFitchRating()).isEqualTo(updatedRatingDTO.getFitchRating());
        assertThat(actualRatingDTO.getMoodysRating()).isEqualTo(updatedRatingDTO.getMoodysRating());
        assertThat(actualRatingDTO.getsAndPRating()).isEqualTo(updatedRatingDTO.getsAndPRating());
    }

    @Test
    void updateRating_ShouldReturnError() {
        //GIVEN
        when(ratingRepository.save(any(Rating.class))).thenThrow(new RuntimeException());
        //WHEN
        assertThatThrownBy(() -> ratingServices.updateRating(updatedRatingDTO))
                .isInstanceOf(PoseidonRuntimeException.class)
                .hasMessage("error while updating rating with id # 1");
    }

    @Test
    void deleteRating_ShouldReturnOk() {
        //GIVEN
        when(ratingRepository.save(any(Rating.class))).thenReturn(updatedRating);
        ratingServices.addRating(updatedRatingDTO);
        //WHEN
        ratingServices.deleteRating(updatedRatingDTO.getId());
        //THEN
        List<RatingDTO> ratingList = ratingServices.getRatingList();
        assertThat(ratingList).isEmpty();
    }

    @Test
    void deleteRating_ShouldReturnError() {
        //GIVEN
        when(ratingRepository.getById(1)).thenReturn(updatedRating);
        doThrow(new RuntimeException()).when(ratingRepository).delete(updatedRating);
        //WHEN
        assertThatThrownBy(() -> ratingServices.deleteRating(1))
                .isInstanceOf(PoseidonRuntimeException.class)
                .hasMessage("error while deleting rating with id # 1");
    }

}
