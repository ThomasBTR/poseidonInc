package inc.poseidon.services;

import inc.poseidon.domain.CurvePoint;
import inc.poseidon.domain.CurvePointDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.repositories.CurvePointRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class CurvePointServicesTest {


    @Autowired
    CurvePointServices curvePointServices;
    @MockBean
    CurvePointRepository curvePointRepository;

    CurvePoint expectedCurvePoint;
    CurvePoint updatedCurvePoint;

    CurvePointDTO expectedCurvePointDTO;

    CurvePointDTO updatedCurvePointDTO;


    @BeforeEach
    void prepare() {
        expectedCurvePoint =new CurvePoint(1, 10d, 10d);
        updatedCurvePoint = new CurvePoint(2, 20d, 100d);
        expectedCurvePointDTO = new CurvePointDTO();
        expectedCurvePointDTO.setId(1);
        expectedCurvePointDTO.setTerm(10d);
        expectedCurvePointDTO.setValue(10d);

        updatedCurvePointDTO = new CurvePointDTO();
        updatedCurvePointDTO.setId(2);
        updatedCurvePointDTO.setTerm(20d);
        updatedCurvePointDTO.setValue(100d);
    }

    @Test
    void getCurvePointList_ShouldReturnOK() {
        //GIVEN
        List<CurvePoint> curvePoints = new ArrayList<>();
        curvePoints.add(expectedCurvePoint);
        List<CurvePointDTO> curvePointDTOList = new ArrayList<>();
        curvePointDTOList.add(expectedCurvePointDTO);
        when(curvePointRepository.findAll()).thenReturn(curvePoints);
        //WHEN
        List<CurvePointDTO> actualBidList = curvePointServices.getCurvePointList();
        //THEN
        assertThat(actualBidList).isEqualTo(curvePointDTOList)
                .hasSize(1)
                .contains(expectedCurvePointDTO);
    }


    @Test
    void getCurvePoint_ShouldReturnOk() {
        //GIVEN
        when(curvePointRepository.save(any(CurvePoint.class))).thenReturn(expectedCurvePoint);
        when(curvePointRepository.getById(1)).thenReturn(expectedCurvePoint);
        curvePointServices.addCurvePoint(expectedCurvePointDTO);
        //WHEN
        CurvePointDTO gatheredCP = curvePointServices.getCurvePoint(expectedCurvePointDTO.getId());
        assertThat(gatheredCP).isEqualTo(expectedCurvePointDTO);
    }

    @Test
    void getCurvePoint_ShouldReturnKO_IdNotInDatabase() {
        //GIVEN
        when(curvePointRepository.save(any(CurvePoint.class))).thenReturn(expectedCurvePoint);
        when(curvePointRepository.getById(2)).thenThrow(new RuntimeException());
        curvePointServices.addCurvePoint(expectedCurvePointDTO);
        //WHEN
        assertThatThrownBy(() -> curvePointServices.getCurvePoint(2)).isInstanceOf(PoseidonRuntimeException.class)
                .hasMessage("Error while getting curve point # 2");
    }

    @Test
    void addv_shouldReturnCurvePoint() {
        //GIVEN
        when(curvePointRepository.save(any(CurvePoint.class))).thenReturn(expectedCurvePoint);
        //WHEN
        CurvePointDTO actualCurvePointDTO = curvePointServices.addCurvePoint(expectedCurvePointDTO);
        //THEN
        assertThat(actualCurvePointDTO).isEqualTo(expectedCurvePointDTO);
        assertThat(actualCurvePointDTO.getId()).isEqualTo(expectedCurvePointDTO.getId());
        assertThat(actualCurvePointDTO.getTerm()).isEqualTo(expectedCurvePointDTO.getTerm());
        assertThat(actualCurvePointDTO.getValue()).isEqualTo(expectedCurvePointDTO.getValue());
    }


    @Test
    void addCurvePoint_shouldReturnError() {
        //GIVEN
        when(curvePointRepository.save(any(CurvePoint.class))).thenThrow(new RuntimeException());
        //WHEN
        assertThatThrownBy(() -> curvePointServices.addCurvePoint(expectedCurvePointDTO))
                .isInstanceOf(PoseidonRuntimeException.class)
                .hasMessage("Error while adding curve point with curvePointId # 1");
    }

    @Test
    void updateCurvePoint_ShouldReturnOk() {
        //GIVEN
        when(curvePointRepository.save(any(CurvePoint.class))).thenReturn(updatedCurvePoint);
        //WHEN
        CurvePointDTO actualCurvePointDTO = curvePointServices.updateCurvePoint(updatedCurvePointDTO);
        //THEN
        assertThat(actualCurvePointDTO).isEqualTo(updatedCurvePointDTO);
        assertThat(actualCurvePointDTO.getId()).isEqualTo(updatedCurvePointDTO.getId());
        assertThat(actualCurvePointDTO.getValue()).isEqualTo(updatedCurvePointDTO.getValue());
        assertThat(actualCurvePointDTO.getTerm()).isEqualTo(updatedCurvePointDTO.getTerm());
    }

    @Test
    void updateCurvePoint_ShouldReturnError() {
        //GIVEN
        when(curvePointRepository.save(any(CurvePoint.class))).thenThrow(new RuntimeException());
        //WHEN
        assertThatThrownBy(() -> curvePointServices.updateCurvePoint(updatedCurvePointDTO))
                .isInstanceOf(PoseidonRuntimeException.class)
                .hasMessage("error while updating curve point id # 2");
    }

    @Test
    void deleteCurvePoint_ShouldReturnOk() {
        //GIVEN
        when(curvePointRepository.save(any(CurvePoint.class))).thenReturn(updatedCurvePoint);
        curvePointServices.addCurvePoint(updatedCurvePointDTO);
        //WHEN
        curvePointServices.deleteCurvePoint(updatedCurvePointDTO.getId());
        //THEN
        List<CurvePointDTO> curvePointList = curvePointServices.getCurvePointList();
        assertThat(curvePointList).isEmpty();
    }

    @Test
    void deleteCurvePoint_ShouldReturnError() {
        //GIVEN
        when(curvePointRepository.getById(1)).thenReturn(updatedCurvePoint);
        doThrow(new RuntimeException()).when(curvePointRepository).delete(updatedCurvePoint);
        //WHEN
        assertThatThrownBy(() -> curvePointServices.deleteCurvePoint(1))
                .isInstanceOf(PoseidonRuntimeException.class)
                .hasMessage("error while deleting curve point id # 1");
    }

}
