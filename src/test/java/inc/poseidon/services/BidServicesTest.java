package inc.poseidon.services;

import inc.poseidon.domain.Bid;
import inc.poseidon.domain.BidDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.repositories.BidListRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class BidServicesTest {


    @Autowired
    BidServices bidServices;
    @MockBean
    BidListRepository bidListRepository;

    Bid expectedBid;
    Bid updatedBid;

    BidDTO bidDTO;

    BidDTO updatedBidDTO;


    @BeforeEach
    void prepare() {
        expectedBid = new Bid(1, "Account_Test", "Type_Test", 10d);
        updatedBid = new Bid(1, "account_updated", "updated_type", 100d);
        bidDTO = new BidDTO();
        bidDTO.setType("Type_Test");
        bidDTO.setAccount("Account_Test");
        bidDTO.setBidQuantity(10d);
        bidDTO.setId(1);

        updatedBidDTO = new BidDTO();
        updatedBidDTO.setType("updated_type");
        updatedBidDTO.setAccount("account_updated");
        updatedBidDTO.setBidQuantity(100d);
        updatedBidDTO.setId(1);
    }

    @Test
    void getAllBidList() {
        //GIVEN
        List<Bid> expectedBids = new ArrayList<>();
        expectedBids.add(expectedBid);
        List<BidDTO> expectedBidDTOs = new ArrayList<>();
        expectedBidDTOs.add(bidDTO);
        when(bidListRepository.findAll()).thenReturn(expectedBids);
        //WHEN
        List<BidDTO> actualBidList = bidServices.getAllBidLists();
        //THEN
        assertThat(actualBidList).isEqualTo(expectedBidDTOs)
                .hasSize(1)
                .contains(bidDTO);
    }


    @Test
    void getBid_ShouldReturnOk() {
        //GIVEN
        when(bidListRepository.save(any(Bid.class))).thenReturn(expectedBid);
        when(bidListRepository.getById(1)).thenReturn(expectedBid);
        BidDTO expectedBidDTO = bidServices.addBidListToList(bidDTO);
        //WHEN
        BidDTO gatheredBid = bidServices.getBidList(expectedBidDTO.getId());
        assertThat(gatheredBid).isEqualTo(expectedBidDTO);
    }

    @Test
    void getBid_ShouldReturnKO_IdNotInDatabase() {
        //GIVEN
        when(bidListRepository.save(any(Bid.class))).thenReturn(expectedBid);
        when(bidListRepository.getById(2)).thenThrow(new RuntimeException());
        bidServices.addBidListToList(bidDTO);
        //WHEN
        assertThatThrownBy(() -> bidServices.getBidList(2)).isInstanceOf(PoseidonRuntimeException.class)
                .hasMessage("Error while getting bid List # 2");
    }

    @Test
    void addBidToBidList_shouldReturnBid() {
        //GIVEN
        when(bidListRepository.save(any(Bid.class))).thenReturn(expectedBid);
        //WHEN
        BidDTO actualBidList = bidServices.addBidListToList(bidDTO);
        //THEN
        assertThat(actualBidList).isEqualTo(bidDTO);
        assertThat(actualBidList.getAccount()).isEqualTo(bidDTO.getAccount());
        assertThat(actualBidList.getBidQuantity()).isEqualTo(bidDTO.getBidQuantity());
        assertThat(actualBidList.getType()).isEqualTo(bidDTO.getType());
    }


    @Test
    void addBidToBidList_shouldReturnError() {
        //GIVEN
        when(bidListRepository.save(any(Bid.class))).thenThrow(new RuntimeException());
        //WHEN
        assertThatThrownBy(() -> bidServices.addBidListToList(bidDTO))
                .isInstanceOf(PoseidonRuntimeException.class)
                .hasMessage("Error while adding bid List with account field Account_Test");
    }

    @Test
    void updateBid_ShouldReturnOk() {
        //GIVEN
        when(bidListRepository.save(any(Bid.class))).thenReturn(updatedBid);
        //WHEN
        BidDTO actualBid = bidServices.updateBidList(updatedBidDTO);
        //THEN
        assertThat(actualBid).isEqualTo(updatedBidDTO);
        assertThat(actualBid.getBidQuantity()).isEqualTo(updatedBidDTO.getBidQuantity());
        assertThat(actualBid.getId()).isEqualTo(updatedBidDTO.getId());
        assertThat(actualBid.getAccount()).isEqualTo(updatedBidDTO.getAccount());
        assertThat(actualBid.getType()).isEqualTo(updatedBidDTO.getType());
    }

    @Test
    void updateBid_ShouldReturnError() {
        //GIVEN
        when(bidListRepository.save(any(Bid.class))).thenThrow(new RuntimeException());
        //WHEN
        assertThatThrownBy(() -> bidServices.updateBidList(updatedBidDTO))
                .isInstanceOf(PoseidonRuntimeException.class)
                .hasMessage("error while updating bid list # 1");
    }

    @Test
    void deleteBid_ShouldReturnOk() {
        //GIVEN
        when(bidListRepository.save(any(Bid.class))).thenReturn(updatedBid);
        bidServices.addBidListToList(updatedBidDTO);
        //WHEN
        bidServices.deleteBidList(updatedBidDTO.getId());
        //THEN
        List<BidDTO> bidDTOList = bidServices.getAllBidLists();
        assertThat(bidDTOList).isEmpty();
    }

    @Test
    void deleteBid_ShouldReturnError() {
        //GIVEN
        when(bidListRepository.getById(1)).thenReturn(expectedBid);
        doThrow(new RuntimeException()).when(bidListRepository).delete(expectedBid);
        //WHEN
        assertThatThrownBy(() -> bidServices.deleteBidList(1))
                .isInstanceOf(PoseidonRuntimeException.class)
                .hasMessage("error while deleting bid list # 1");
    }

}
