package inc.poseidon.services;

import inc.poseidon.domain.RuleName;
import inc.poseidon.domain.RuleNameDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.repositories.RuleNameRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class RuleNameServicesTest {


    @Autowired
    RuleNameServices ruleNameServices;
    @MockBean
    RuleNameRepository ruleNameRepository;

    RuleName expectedRuleName;
    RuleName updatedRuleName;

    RuleNameDTO expectedRuleNameDTO;

    RuleNameDTO updatedRuleNameDTO;


    @BeforeEach
    void prepare() {
        expectedRuleName = new RuleName(0, "ruleA", "the rule type A", null, null, null, null);
        updatedRuleName = new RuleName(0, "ruleB", "the rule type B", null, null, null, null);

        expectedRuleNameDTO = new RuleNameDTO();
        expectedRuleNameDTO.setId(0);
        expectedRuleNameDTO.setName("ruleA");
        expectedRuleNameDTO.setDescription("the rule type A");

        updatedRuleNameDTO = new RuleNameDTO();
        updatedRuleNameDTO.setId(0);
        updatedRuleNameDTO.setName("ruleB");
        updatedRuleNameDTO.setDescription("the rule type B");
    }

    @Test
    void getRuleNameList_ShouldReturnOK() {
        //GIVEN
        List<RuleName> ruleNames = new ArrayList<>();
        ruleNames.add(expectedRuleName);
        List<RuleNameDTO> ruleNameDTOList = new ArrayList<>();
        ruleNameDTOList.add(expectedRuleNameDTO);
        when(ruleNameRepository.findAll()).thenReturn(ruleNames);
        //WHEN
        List<RuleNameDTO> actualRuleNameDTOList = ruleNameServices.getRuleNameList();
        //THEN
        assertThat(actualRuleNameDTOList).isEqualTo(ruleNameDTOList).hasSize(1).contains(expectedRuleNameDTO);
    }


    @Test
    void getRuleName_ShouldReturnOk() {
        //GIVEN
        when(ruleNameRepository.save(any(RuleName.class))).thenReturn(expectedRuleName);
        when(ruleNameRepository.getById(0)).thenReturn(expectedRuleName);
        ruleNameServices.addRuleName(expectedRuleNameDTO);
        //WHEN
        RuleNameDTO gatheredRuleName = ruleNameServices.getRuleName(expectedRuleNameDTO.getId());
        assertThat(gatheredRuleName).isEqualTo(expectedRuleNameDTO);
    }

    @Test
    void getRuleName_ShouldReturnKO_IdNotInDatabase() {
        //GIVEN
        when(ruleNameRepository.save(any(RuleName.class))).thenReturn(expectedRuleName);
        when(ruleNameRepository.getById(2)).thenThrow(new RuntimeException());
        ruleNameServices.addRuleName(expectedRuleNameDTO);
        //WHEN
        assertThatThrownBy(() -> ruleNameServices.getRuleName(2)).isInstanceOf(PoseidonRuntimeException.class).hasMessage("Error while getting ruleName with id # 2");
    }

    @Test
    void add_shouldReturnRuleName() {
        //GIVEN
        when(ruleNameRepository.save(any(RuleName.class))).thenReturn(expectedRuleName);
        //WHEN
        RuleNameDTO ruleNameDTO = ruleNameServices.addRuleName(expectedRuleNameDTO);
        //THEN
        assertThat(ruleNameDTO).isEqualTo(expectedRuleNameDTO);
        assertThat(ruleNameDTO.getId()).isEqualTo(expectedRuleNameDTO.getId());
        assertThat(ruleNameDTO.getName()).isEqualTo(expectedRuleNameDTO.getName());
        assertThat(ruleNameDTO.getDescription()).isEqualTo(expectedRuleNameDTO.getDescription());
        assertThat(ruleNameDTO.getSqlPart()).isEqualTo(expectedRuleNameDTO.getSqlPart());
        assertThat(ruleNameDTO.getSqlStr()).isEqualTo(expectedRuleNameDTO.getSqlStr());
        assertThat(ruleNameDTO.getTemplate()).isEqualTo(expectedRuleNameDTO.getTemplate());
    }


    @Test
    void addRuleName_shouldReturnError() {
        //GIVEN
        when(ruleNameRepository.save(any(RuleName.class))).thenThrow(new RuntimeException());
        //WHEN
        assertThatThrownBy(() -> ruleNameServices.addRuleName(expectedRuleNameDTO)).isInstanceOf(PoseidonRuntimeException.class).hasMessage("Error while adding ruleName with id # 0");
    }

    @Test
    void updateRuleName_ShouldReturnOk() {
        //GIVEN
        when(ruleNameRepository.save(any(RuleName.class))).thenReturn(updatedRuleName);
        //WHEN
        RuleNameDTO updateRuleName = ruleNameServices.updateRuleName(updatedRuleNameDTO);
        //THEN
        assertThat(updateRuleName).isEqualTo(updatedRuleNameDTO);
        assertThat(updateRuleName.getId()).isEqualTo(updatedRuleNameDTO.getId());
        assertThat(updateRuleName.getName()).isEqualTo(updatedRuleNameDTO.getName());
        assertThat(updateRuleName.getRuleNameJson()).isEqualTo(updatedRuleNameDTO.getRuleNameJson());
        assertThat(updateRuleName.getSqlPart()).isEqualTo(updatedRuleNameDTO.getSqlPart());
        assertThat(updateRuleName.getTemplate()).isEqualTo(updatedRuleNameDTO.getTemplate());
        assertThat(updateRuleName.getSqlStr()).isEqualTo(updatedRuleNameDTO.getSqlStr());
        assertThat(updateRuleName.getDescription()).isEqualTo(updatedRuleNameDTO.getDescription());
    }

    @Test
    void updateRuleName_ShouldReturnError() {
        //GIVEN
        when(ruleNameRepository.save(any(RuleName.class))).thenThrow(new RuntimeException());
        //WHEN
        assertThatThrownBy(() -> ruleNameServices.updateRuleName(updatedRuleNameDTO)).isInstanceOf(PoseidonRuntimeException.class).hasMessage("error while updating ruleName with id # 0");
    }

    @Test
    void deleteRuleName_ShouldReturnOk() {
        //GIVEN
        when(ruleNameRepository.save(any(RuleName.class))).thenReturn(updatedRuleName);
        ruleNameServices.addRuleName(updatedRuleNameDTO);
        //WHEN
        ruleNameServices.deleteRuleName(updatedRuleNameDTO.getId());
        //THEN
        List<RuleNameDTO> ratingList = ruleNameServices.getRuleNameList();
        assertThat(ratingList).isEmpty();
    }

    @Test
    void deleteRuleName_ShouldReturnError() {
        //GIVEN
        when(ruleNameRepository.getById(0)).thenReturn(updatedRuleName);
        doThrow(new RuntimeException()).when(ruleNameRepository).delete(updatedRuleName);
        //WHEN
        assertThatThrownBy(() -> ruleNameServices.deleteRuleName(0)).isInstanceOf(PoseidonRuntimeException.class).hasMessage("error while deleting ruleName with id # 0");
    }

}
