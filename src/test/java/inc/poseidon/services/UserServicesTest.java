package inc.poseidon.services;

import inc.poseidon.domain.User;
import inc.poseidon.domain.UserDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class UserServicesTest {


    @Autowired
    UserServices userServices;
    @MockBean
    UserRepository userRepository;

    User expectedUser;
    User updatedUser;

    UserDTO expectedUserDTO;

    UserDTO updatedUserDTO;


    @BeforeEach
    void prepare() {
        expectedUser = new User(0, "accountA", "Pa$$worD");
        updatedUser = new User(0, "@Ccount", "Pa$$worD");

        expectedUserDTO = new UserDTO();
        expectedUserDTO.setId(0);
        expectedUserDTO.setUsername("accountA");
        expectedUserDTO.setPassword("Pa$$worD");
        expectedUserDTO.setFullName("");
        expectedUserDTO.setRole("ROLE_USER");

        updatedUserDTO = new UserDTO();
        updatedUserDTO.setId(0);
        updatedUserDTO.setUsername("@Ccount");
        updatedUserDTO.setPassword("Pa$$worD");
        updatedUserDTO.setFullName("");
        updatedUserDTO.setRole("ROLE_USER");
    }

    @Test
    void getUserList_ShouldReturnOK() {
        //GIVEN
        List<User> Users = new ArrayList<>();
        Users.add(expectedUser);
        List<UserDTO> UserDTOS = new ArrayList<>();
        UserDTOS.add(expectedUserDTO);
        when(userRepository.findAll()).thenReturn(Users);
        //WHEN
        List<UserDTO> actualUserDTOList = userServices.getUserList();
        //THEN
        assertThat(actualUserDTOList).isEqualTo(UserDTOS).hasSize(1).contains(expectedUserDTO);
    }


    @Test
    void getUser_ShouldReturnOk() {
        //GIVEN
        when(userRepository.save(any(User.class))).thenReturn(expectedUser);
        when(userRepository.getById(0)).thenReturn(expectedUser);
        userServices.addUser(expectedUserDTO);
        //WHEN
        UserDTO gatheredUser = userServices.getUser(expectedUserDTO.getId());
        assertThat(gatheredUser).isEqualTo(expectedUserDTO);
    }

    @Test
    void getUser_ShouldReturnKO_IdNotInDatabase() {
        //GIVEN
        when(userRepository.save(any(User.class))).thenReturn(expectedUser);
        when(userRepository.getById(2)).thenThrow(new RuntimeException());
        userServices.addUser(expectedUserDTO);
        //WHEN
        assertThatThrownBy(() -> userServices.getUser(2)).isInstanceOf(PoseidonRuntimeException.class).hasMessage("Error while getting user with id # 2");
    }

    @Test
    void add_shouldReturnUser() {
        //GIVEN
        when(userRepository.save(any(User.class))).thenReturn(expectedUser);
        //WHEN
        UserDTO UserDTO = userServices.addUser(expectedUserDTO);
        //THEN
        assertThat(UserDTO).isEqualTo(expectedUserDTO);
        assertThat(UserDTO.getId()).isEqualTo(expectedUserDTO.getId());
        assertThat(UserDTO.getUsername()).isEqualTo(expectedUserDTO.getUsername());
        assertThat(UserDTO.getPassword()).isEqualTo(expectedUserDTO.getPassword());
        assertThat(UserDTO.getRole()).isEqualTo(expectedUserDTO.getRole());
        assertThat(UserDTO.getFullName()).isEqualTo(expectedUserDTO.getFullName());
    }


    @Test
    void addUser_shouldReturnError() {
        //GIVEN
        when(userRepository.save(any(User.class))).thenThrow(new RuntimeException());
        //WHEN
        assertThatThrownBy(() -> userServices.addUser(expectedUserDTO)).isInstanceOf(PoseidonRuntimeException.class).hasMessage("Error while adding user with id # 0");
    }

    @Test
    void updateUser_ShouldReturnOk() {
        //GIVEN
        when(userRepository.save(any(User.class))).thenReturn(updatedUser);
        //WHEN
        UserDTO updatedUser = userServices.updateUser(updatedUserDTO);
        //THEN
        assertThat(updatedUser).isEqualTo(updatedUserDTO);
        assertThat(updatedUser.getId()).isEqualTo(updatedUserDTO.getId());
        assertThat(updatedUser.getUsername()).isEqualTo(updatedUserDTO.getUsername());
        assertThat(updatedUser.getPassword()).isEqualTo(updatedUserDTO.getPassword());
        assertThat(updatedUser.getFullName()).isEqualTo(updatedUserDTO.getFullName());
        assertThat(updatedUser.getRole()).isEqualTo(updatedUserDTO.getRole());
    }

    @Test
    void updateUser_ShouldReturnError() {
        //GIVEN
        when(userRepository.save(any(User.class))).thenThrow(new RuntimeException());
        //WHEN
        assertThatThrownBy(() -> userServices.updateUser(updatedUserDTO)).isInstanceOf(PoseidonRuntimeException.class).hasMessage("error while updating user with id # 0");
    }

    @Test
    void deleteUser_ShouldReturnOk() {
        //GIVEN
        when(userRepository.save(any(User.class))).thenReturn(updatedUser);
        userServices.addUser(updatedUserDTO);
        //WHEN
        userServices.deleteUser(updatedUserDTO.getId());
        //THEN
        List<UserDTO> UserList = userServices.getUserList();
        assertThat(UserList).isEmpty();
    }

    @Test
    void deleteUser_ShouldReturnError() {
        //GIVEN
        when(userRepository.getById(0)).thenReturn(updatedUser);
        doThrow(new RuntimeException()).when(userRepository).delete(updatedUser);
        //WHEN
        assertThatThrownBy(() -> userServices.deleteUser(0)).isInstanceOf(PoseidonRuntimeException.class).hasMessage("error while deleting user with id # 0");
    }

}
