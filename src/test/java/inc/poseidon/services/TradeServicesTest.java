package inc.poseidon.services;

import inc.poseidon.domain.Trade;
import inc.poseidon.domain.TradeDTO;
import inc.poseidon.exceptions.PoseidonRuntimeException;
import inc.poseidon.repositories.TradeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class TradeServicesTest {


    @Autowired
    TradeServices tradeServices;
    @MockBean
    TradeRepository tradeRepository;

    Trade expectedTrade;
    Trade updatedTrade;

    TradeDTO expectedTradeDTO;

    TradeDTO updatedTradeDTO;


    @BeforeEach
    void prepare() {
        expectedTrade = new Trade(0, "accountA", "tradingTypeA", Instant.parse("2022-06-13T11:50:39Z"));
        updatedTrade = new Trade(0, "accountA", "tradingTypeA", 10d, 20d, 1552d, 10541d, Instant.parse("2022-06-13T11:50:39Z"));

        expectedTradeDTO = new TradeDTO();
        expectedTradeDTO.setId(0);
        expectedTradeDTO.setAccount("accountA");
        expectedTradeDTO.setType("tradingTypeA");
        expectedTradeDTO.setBuyQuantity(0d);
        expectedTradeDTO.setSellQuantity(0d);
        expectedTradeDTO.setBuyPrice(0d);
        expectedTradeDTO.setSellPrice(0d);
        expectedTradeDTO.setTradeDate(Instant.parse("2022-06-13T11:50:39Z"));

        updatedTradeDTO = new TradeDTO();
        updatedTradeDTO.setId(0);
        updatedTradeDTO.setAccount("accountA");
        updatedTradeDTO.setType("tradingTypeA");
        updatedTradeDTO.setBuyQuantity(10d);
        updatedTradeDTO.setSellQuantity(20d);
        updatedTradeDTO.setBuyPrice(1552d);
        updatedTradeDTO.setSellPrice(10541d);
        updatedTradeDTO.setTradeDate(Instant.parse("2022-06-13T11:50:39Z"));


    }

    @Test
    void getTradeList_ShouldReturnOK() {
        //GIVEN
        List<Trade> trades = new ArrayList<>();
        trades.add(expectedTrade);
        List<TradeDTO> tradeDTOS = new ArrayList<>();
        tradeDTOS.add(expectedTradeDTO);
        when(tradeRepository.findAll()).thenReturn(trades);
        //WHEN
        List<TradeDTO> actualTradeDTOList = tradeServices.getTradeList();
        //THEN
        assertThat(actualTradeDTOList).isEqualTo(tradeDTOS).hasSize(1).contains(expectedTradeDTO);
    }


    @Test
    void getTrade_ShouldReturnOk() {
        //GIVEN
        when(tradeRepository.save(any(Trade.class))).thenReturn(expectedTrade);
        when(tradeRepository.getById(0)).thenReturn(expectedTrade);
        tradeServices.addTrade(expectedTradeDTO);
        //WHEN
        TradeDTO gatheredTrade = tradeServices.getTrade(expectedTradeDTO.getId());
        assertThat(gatheredTrade).isEqualTo(expectedTradeDTO);
    }

    @Test
    void getTrade_ShouldReturnKO_IdNotInDatabase() {
        //GIVEN
        when(tradeRepository.save(any(Trade.class))).thenReturn(expectedTrade);
        when(tradeRepository.getById(2)).thenThrow(new RuntimeException());
        tradeServices.addTrade(expectedTradeDTO);
        //WHEN
        assertThatThrownBy(() -> tradeServices.getTrade(2)).isInstanceOf(PoseidonRuntimeException.class).hasMessage("Error while getting trade with id # 2");
    }

    @Test
    void add_shouldReturnTrade() {
        //GIVEN
        when(tradeRepository.save(any(Trade.class))).thenReturn(expectedTrade);
        //WHEN
        TradeDTO tradeDTO = tradeServices.addTrade(expectedTradeDTO);
        //THEN
        assertThat(tradeDTO).isEqualTo(expectedTradeDTO);
        assertThat(tradeDTO.getId()).isEqualTo(expectedTradeDTO.getId());
        assertThat(tradeDTO.getAccount()).isEqualTo(expectedTradeDTO.getAccount());
        assertThat(tradeDTO.getType()).isEqualTo(expectedTradeDTO.getType());
    }


    @Test
    void addTrade_shouldReturnError() {
        //GIVEN
        when(tradeRepository.save(any(Trade.class))).thenThrow(new RuntimeException());
        //WHEN
        assertThatThrownBy(() -> tradeServices.addTrade(expectedTradeDTO)).isInstanceOf(PoseidonRuntimeException.class).hasMessage("Error while adding trade with id # 0");
    }

    @Test
    void updateTrade_ShouldReturnOk() {
        //GIVEN
        when(tradeRepository.save(any(Trade.class))).thenReturn(updatedTrade);
        //WHEN
        TradeDTO updatedTrade = tradeServices.updateTrade(updatedTradeDTO);
        //THEN
        assertThat(updatedTrade).isEqualTo(updatedTradeDTO);
        assertThat(updatedTrade.getId()).isEqualTo(updatedTradeDTO.getId());
        assertThat(updatedTrade.getAccount()).isEqualTo(updatedTradeDTO.getAccount());
        assertThat(updatedTrade.getType()).isEqualTo(updatedTradeDTO.getType());
        assertThat(updatedTrade.getBuyQuantity()).isEqualTo(updatedTradeDTO.getBuyQuantity());
        assertThat(updatedTrade.getSellQuantity()).isEqualTo(updatedTradeDTO.getSellQuantity());
        assertThat(updatedTrade.getBuyPrice()).isEqualTo(updatedTradeDTO.getBuyPrice());
        assertThat(updatedTrade.getSellPrice()).isEqualTo(updatedTradeDTO.getSellPrice());
        assertThat(updatedTrade.getTradeDate()).isEqualTo(updatedTradeDTO.getTradeDate());
    }

    @Test
    void updateTrade_ShouldReturnError() {
        //GIVEN
        when(tradeRepository.save(any(Trade.class))).thenThrow(new RuntimeException());
        //WHEN
        assertThatThrownBy(() -> tradeServices.updateTrade(updatedTradeDTO)).isInstanceOf(PoseidonRuntimeException.class).hasMessage("error while updating trade with id # 0");
    }

    @Test
    void deleteTrade_ShouldReturnOk() {
        //GIVEN
        when(tradeRepository.save(any(Trade.class))).thenReturn(updatedTrade);
        tradeServices.addTrade(updatedTradeDTO);
        //WHEN
        tradeServices.deleteTrade(updatedTradeDTO.getId());
        //THEN
        List<TradeDTO> tradeList = tradeServices.getTradeList();
        assertThat(tradeList).isEmpty();
    }

    @Test
    void deleteTrade_ShouldReturnError() {
        //GIVEN
        when(tradeRepository.getById(0)).thenReturn(updatedTrade);
        doThrow(new RuntimeException()).when(tradeRepository).delete(updatedTrade);
        //WHEN
        assertThatThrownBy(() -> tradeServices.deleteTrade(0)).isInstanceOf(PoseidonRuntimeException.class).hasMessage("error while deleting trade with id # 0");
    }

}
