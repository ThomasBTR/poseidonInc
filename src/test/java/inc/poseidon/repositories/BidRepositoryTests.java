package inc.poseidon.repositories;

import inc.poseidon.domain.Bid;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class BidRepositoryTests {

    @Autowired
    private BidListRepository bidListRepository;

    Bid bidTest;

    @BeforeEach
    void prepare() {
        // password = "@Toto2022" with BcryptEncryption
        Instant date = Instant.parse("2022-06-11T10:10:01Z");
        bidTest = new Bid(5, "totoAccount", "typeA", 20d, 10d, 200d, 22d, "benchValue", date, "nothing to Add", "ok",
                "ok", "Robert", "Robert's Book", "totoBid", date, "totoBid", date, "dealA", "typeDealAB",
                "sourceListB", "sideD");
    }


    @Test
    @Order(1)
    void findAll_ReturnTrue() {
        List<Bid> bidList = bidListRepository.findAll();
        assertThat(bidList).hasSize(5);
    }


    @Test
    @Order(2)
    void save_retrieve_validateToto() {
        //GIVEN
        //WHEN
        Bid bidSavedInDb = bidListRepository.save(bidTest);
        //THEN
        assertThat(bidSavedInDb).isEqualTo(bidTest);
        assertThat(bidSavedInDb.toString()).hasToString(bidTest.toString());
        assertThat(bidSavedInDb.hashCode()).hasSameHashCodeAs(bidTest.hashCode());
        assertThat(bidSavedInDb.getBidId()).isEqualTo(bidTest.getBidId());
        assertThat(bidSavedInDb.getBidQuantity()).isEqualTo(bidTest.getBidQuantity());
        assertThat(bidSavedInDb.getBidValue()).isEqualTo(bidTest.getBidValue());
        assertThat(bidSavedInDb.getAccount()).isEqualTo(bidTest.getAccount());
        assertThat(bidSavedInDb.getType()).isEqualTo(bidTest.getType());
        assertThat(bidSavedInDb.getBidListDate()).isEqualTo(bidTest.getBidListDate());
        assertThat(bidSavedInDb.getAsk()).isEqualTo(bidTest.getAsk());
        assertThat(bidSavedInDb.getAskQuantity()).isEqualTo(bidTest.getAskQuantity());
        assertThat(bidSavedInDb.getBenchmark()).isEqualTo(bidTest.getBenchmark());
        assertThat(bidSavedInDb.getBook()).isEqualTo(bidTest.getBook());
        assertThat(bidSavedInDb.getCommentary()).isEqualTo(bidTest.getCommentary());
        assertThat(bidSavedInDb.getCreationDate()).isEqualTo(bidTest.getCreationDate());
        assertThat(bidSavedInDb.getCreationName()).isEqualTo(bidTest.getCreationName());
        assertThat(bidSavedInDb.getRevisionDate()).isEqualTo(bidTest.getRevisionDate());
        assertThat(bidSavedInDb.getDealName()).isEqualTo(bidTest.getDealName());
        assertThat(bidSavedInDb.getDealType()).isEqualTo(bidTest.getDealType());
        assertThat(bidSavedInDb.getSecurity()).isEqualTo(bidTest.getSecurity());
        assertThat(bidSavedInDb.getSide()).isEqualTo(bidTest.getSide());
        assertThat(bidSavedInDb.getSourceListId()).isEqualTo(bidTest.getSourceListId());
        assertThat(bidSavedInDb.getStatus()).isEqualTo(bidTest.getStatus());
        assertThat(bidSavedInDb.getTrader()).isEqualTo(bidTest.getTrader());
    }

    @Test
    @Order(3)
    void save_update_validateToto() {
        //GIVEN
        bidListRepository.save(bidTest);
        String side = "Cside";
        bidTest.setSide(side);
        //WHEN
        Bid bidUpdated = bidListRepository.save(bidTest);
        //THEN
        assertThat(bidUpdated).isEqualTo(bidTest);
        assertThat(bidUpdated.getSide()).isEqualTo(side);
    }

    @Test
    @Order(4)
    void delete_returnOK() {
        //GIVEN
        Bid bid0 = bidListRepository.getById(0);
        //WHEN
        bidListRepository.delete(bid0);
        //THEN
        assertThat(bidListRepository.findAll()).hasSize(4);
        assertThat(bidListRepository.findById(bid0.getBidId())).isEmpty();
    }
}
