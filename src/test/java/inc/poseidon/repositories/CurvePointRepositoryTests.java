package inc.poseidon.repositories;

import inc.poseidon.domain.CurvePoint;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class CurvePointRepositoryTests {

	@Autowired
	private CurvePointRepository curvePointRepository;

	@Test
	void curvePointTest() {
		Random r = new Random();
		int randomInt = r.nextInt();
		CurvePoint curvePoint = new CurvePoint(randomInt, 10d, 30d);

		// Save
		curvePoint = curvePointRepository.save(curvePoint);
		Assert.assertNotNull(curvePoint.getId());
		assertThat(curvePoint.getId()).isEqualTo(randomInt);

		// Update
		curvePoint.setTerm(20d);
		curvePoint = curvePointRepository.save(curvePoint);
		assertThat(curvePoint.getTerm()).isEqualTo(20d);

		// Find
		List<CurvePoint> listResult = curvePointRepository.findAll();
		Assert.assertTrue(listResult.size() > 0);

		// Delete
		Integer id = curvePoint.getId();
		curvePointRepository.delete(curvePoint);
		Optional<CurvePoint> curvePointList = curvePointRepository.findById(id);
		Assert.assertFalse(curvePointList.isPresent());
	}

}
