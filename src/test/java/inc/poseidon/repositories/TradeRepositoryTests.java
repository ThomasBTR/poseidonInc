package inc.poseidon.repositories;

import inc.poseidon.domain.Trade;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@DataJpaTest
class TradeRepositoryTests {

    @Autowired
    private TradeRepository tradeRepository;

    @Test
    void tradeTest() {
        Random r = new Random();
        Trade trade = new Trade(r.nextInt(), "Trade Account", "Type");

        // Save
        trade = tradeRepository.save(trade);
        Assert.assertEquals("Trade Account", trade.getAccount());

        // Update
        trade.setAccount("Trade Account Update");
        trade = tradeRepository.save(trade);
        Assert.assertEquals("Trade Account Update", trade.getAccount());

        // Find
        List<Trade> listResult = tradeRepository.findAll();
        Assert.assertTrue(listResult.size() > 0);

        // Delete
        Integer id = trade.getId();
        tradeRepository.delete(trade);
        Optional<Trade> tradeList = tradeRepository.findById(id);
        Assert.assertFalse(tradeList.isPresent());
    }
}
